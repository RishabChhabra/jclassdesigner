/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test_bed;

import javafx.scene.layout.Pane;
import jcd.JClassDesigner;
import jcd.data.ClassObject;
import jcd.data.DataManager;
import jcd.data.MethodObject;
import jcd.data.VariableObject;
import jcd.file.FileManager;
import saf.components.AppComponentsBuilder;

/**
 *
 * @author rishab
 */
public class TestSave {

    public static void main(String[] args) throws Exception {
        DataManager manager;
        FileManager file;

        JClassDesigner designer = new JClassDesigner();
        AppComponentsBuilder builder = designer.makeAppBuilderHook();
        manager = (DataManager) builder.buildDataComponent();
        file = (FileManager) builder.buildFileComponent();

        createDesign(manager);

        file.saveData(manager, "work/DesignSaveTest.jcd");
    }

    private static void createDesign(DataManager manager)
    {
        Pane dummy = new Pane();
        
        ClassObject ThreadExample = new ClassObject(dummy, "class",false);
        ThreadExample.setName("ThreadExample");
        ThreadExample.setPkg("");
        ThreadExample.setX(609);
        ThreadExample.setY(282);
        ThreadExample.setHeight(150);
        ThreadExample.setWidth(150);

        VariableObject var = new VariableObject();
        var.setAcccess("public");
        var.setStatic(true);
        var.setName("START_TEXT");
        var.setType("String");
        ThreadExample.addVariable(var);

        var = new VariableObject();
        var.setAcccess("public");
        var.setStatic(true);
        var.setName("PAUSE_TEXT");
        var.setType("String");
        ThreadExample.addVariable(var);

        var = new VariableObject();
        var.setAcccess("public");
        var.setStatic(true);
        var.setName("START_TEXT");
        var.setType("String");
        ThreadExample.addVariable(var);

        var = new VariableObject();
        var.setAcccess("private");
        var.setStatic(false);
        var.setName("window");
        var.setType("Stage");
        ThreadExample.addVariable(var);

        var = new VariableObject();
        var.setAcccess("private");
        var.setStatic(false);
        var.setName("appPane");
        var.setType("BorderPane");
        ThreadExample.addVariable(var);

        var = new VariableObject();
        var.setAcccess("private");
        var.setStatic(false);
        var.setName("topPane");
        var.setType("FlowPane");
        ThreadExample.addVariable(var);

        var = new VariableObject();
        var.setAcccess("private");
        var.setStatic(false);
        var.setName("startButton");
        var.setType("Button");
        ThreadExample.addVariable(var);

        var = new VariableObject();
        var.setAcccess("private");
        var.setStatic(false);
        var.setName("pauseButton");
        var.setType("Button");
        ThreadExample.addVariable(var);

        var = new VariableObject();
        var.setAcccess("private");
        var.setStatic(false);
        var.setName("scrollPane");
        var.setType("ScrollPane");
        ThreadExample.addVariable(var);

        var = new VariableObject();
        var.setAcccess("private");
        var.setStatic(false);
        var.setName("textArea");
        var.setType("TextArea");
        ThreadExample.addVariable(var);

        var = new VariableObject();
        var.setAcccess("private");
        var.setStatic(false);
        var.setName("dateThread");
        var.setType("Thread");
        ThreadExample.addVariable(var);

        var = new VariableObject();
        var.setAcccess("private");
        var.setStatic(false);
        var.setName("dateTask");
        var.setType("Task");
        ThreadExample.addVariable(var);

        var = new VariableObject();
        var.setAcccess("private");
        var.setStatic(false);
        var.setName("counterThread");
        var.setType("Thread");
        ThreadExample.addVariable(var);

        var = new VariableObject();
        var.setAcccess("private");
        var.setStatic(false);
        var.setName("counterTask");
        var.setType("Task");
        ThreadExample.addVariable(var);

        var = new VariableObject();
        var.setAcccess("private");
        var.setStatic(false);
        var.setName("work");
        var.setType("boolean");
        ThreadExample.addVariable(var);

        MethodObject method = new MethodObject();
        method.setName("start");
        method.setAccess("public");
        method.setStatic(false);
        method.setReturnType("void");
        var = new VariableObject();
        var.setAcccess("arg");
        var.setName("primaryStage");
        var.setStatic(false);
        var.setType("Stage");
        method.addArg(var);
        ThreadExample.addMethod(method);
        
        method = new MethodObject();
        method.setName("startWork");
        method.setAccess("public");
        method.setStatic(false);
        method.setReturnType("void");
        ThreadExample.addMethod(method);

        
        method = new MethodObject();
        method.setName("pauseWork");
        method.setAccess("public");
        method.setStatic(false);
        method.setReturnType("void");  
        ThreadExample.addMethod(method);

        
        method = new MethodObject();
        method.setName("doWork");
        method.setAccess("public");
        method.setStatic(false);
        method.setReturnType("boolean");
        ThreadExample.addMethod(method);
        
        method = new MethodObject();
        method.setName("appendText");
        method.setAccess("public");
        method.setStatic(false);
        method.setReturnType("void");
        var = new VariableObject();
        var.setAcccess("arg");
        var.setName("textToAppend");
        var.setStatic(false);
        var.setType("String");
        method.addArg(var);
        ThreadExample.addMethod(method);
        
        method = new MethodObject();
        method.setName("sleep");
        method.setAccess("public");
        method.setStatic(false);
        method.setReturnType("void");
        var = new VariableObject();
        var.setAcccess("arg");
        var.setName("timeToSleep");
        var.setStatic(false);
        var.setType("int");
        method.addArg(var);
        ThreadExample.addMethod(method);
        
        
        method = new MethodObject();
        method.setName("initLayout");
        method.setAccess("private");
        method.setStatic(false);
        method.setReturnType("void");
        ThreadExample.addMethod(method);
        
        method = new MethodObject();
        method.setName("initHandlers");
        method.setAccess("private");
        method.setStatic(false);
        method.setReturnType("void");
        ThreadExample.addMethod(method);
        
        method = new MethodObject();
        method.setName("initWindow");
        method.setAccess("private");
        method.setStatic(false);
        method.setReturnType("void");
        var = new VariableObject();
        var.setAcccess("arg");
        var.setName("initPrimaryStage");
        var.setStatic(false);
        var.setType("Stage");
        method.addArg(var);
        ThreadExample.addMethod(method);
        
        method = new MethodObject();
        method.setName("initThreads");
        method.setAccess("private");
        method.setStatic(false);
        method.setReturnType("void");
        ThreadExample.addMethod(method);
        
        method = new MethodObject();
        method.setName("main");
        method.setAccess("public");
        method.setStatic(true);
        method.setReturnType("void");
        var = new VariableObject();
        var.setAcccess("arg");
        var.setName("args");
        var.setStatic(false);
        var.setType("String[]");
        method.addArg(var);
        ThreadExample.addMethod(method);
        
        manager.addClass(ThreadExample);
        
        ClassObject Application = new ClassObject(dummy, "abstract",true);
        Application.setName("Application");
        Application.setPkg("javafx");
        Application.setX(603);
        Application.setY(53);
        Application.setHeight(150);
        Application.setWidth(150);
        
        method = new MethodObject();
        method.setName("start");
        method.setAccess("public");
        method.setStatic(false);
        method.setReturnType("void");
        method.setAbstract(true);
        var = new VariableObject();
        var.setAcccess("arg");
        var.setName("primaryStage");
        var.setStatic(false);
        var.setType("Stage");
        method.addArg(var);
        Application.addMethod(method);
        
        manager.addClass(Application);
        
        ClassObject PauseHandler = new ClassObject(dummy, "class",false);
        PauseHandler.setName("PauseHandler");
        PauseHandler.setPkg("");
        PauseHandler.setX(883);
        PauseHandler.setY(188);
        PauseHandler.setHeight(150);
        PauseHandler.setWidth(150);
        
        var = new VariableObject();
        var.setAcccess("private");
        var.setName("app");
        var.setStatic(false);
        var.setType("ThreadExample");
        PauseHandler.addVariable(var);
        
        method = new MethodObject();
        method.setName("PauseHandler");
        method.setAccess("public");
        method.setStatic(false);
        method.setReturnType("constructor");
        var = new VariableObject();
        var.setAcccess("arg");
        var.setName("initApp");
        var.setStatic(false);
        var.setType("ThreadExample");
        method.addArg(var);
        PauseHandler.addMethod(method);
        
        method = new MethodObject();
        method.setName("handle");
        method.setAccess("public");
        method.setStatic(false);
        method.setReturnType("void");
        var = new VariableObject();
        var.setAcccess("arg");
        var.setName("event");
        var.setStatic(false);
        var.setType("Event");
        method.addArg(var);
        PauseHandler.addMethod(method);
        
        manager.addClass(PauseHandler);
        
        ClassObject StartHandler = new ClassObject(dummy, "class",false);
        StartHandler.setName("StartHandler");
        StartHandler.setPkg("");
        StartHandler.setX(891);
        StartHandler.setY(410);
        StartHandler.setHeight(150);
        StartHandler.setWidth(150);
        
        var = new VariableObject();
        var.setAcccess("private");
        var.setName("app");
        var.setStatic(false);
        var.setType("ThreadExample");
        StartHandler.addVariable(var);
        
        method = new MethodObject();
        method.setName("StartHandler");
        method.setAccess("public");
        method.setStatic(false);
        method.setReturnType("constructor");
        var = new VariableObject();
        var.setAcccess("arg");
        var.setName("initApp");
        var.setStatic(false);
        var.setType("ThreadExample");
        method.addArg(var);
        StartHandler.addMethod(method);
        
        method = new MethodObject();
        method.setName("handle");
        method.setAccess("public");
        method.setStatic(false);
        method.setReturnType("void");
        var = new VariableObject();
        var.setAcccess("arg");
        var.setName("event");
        var.setStatic(false);
        var.setType("Event");
        method.addArg(var);
        StartHandler.addMethod(method);
        
        manager.addClass(StartHandler);
        
        ClassObject EventHandler = new ClassObject(dummy, "interface",true);
        EventHandler.setName("EventHandler");
        EventHandler.setPkg("javafx.event");
        EventHandler.setX(1188);
        EventHandler.setY(303);
        EventHandler.setHeight(150);
        EventHandler.setWidth(150);
        
        method = new MethodObject();
        method.setName("handle");
        method.setAccess("public");
        method.setStatic(false);
        method.setReturnType("void");
        var = new VariableObject();
        var.setAcccess("arg");
        var.setName("event");
        var.setStatic(false);
        var.setType("Event");
        method.addArg(var);
        EventHandler.addMethod(method);
        
        manager.addClass(EventHandler);
        
        ClassObject Thread = new ClassObject(dummy, "class",true);
        Thread.setName("Thread");
        Thread.setPkg("java.lang");
        Thread.setX(1183);
        Thread.setY(713);
        Thread.setHeight(150);
        Thread.setWidth(150);
        
        manager.addClass(Thread);
        
        ClassObject Stage = new ClassObject(dummy, "class",true);
        Stage.setName("Stage");
        Stage.setPkg("javafx.stage");
        Stage.setX(88);
        Stage.setY(714);
        Stage.setHeight(150);
        Stage.setWidth(150);
        
        manager.addClass(Stage);
        
        ClassObject BorderPane = new ClassObject(dummy, "class",true);
        BorderPane.setName("BorderPane");
        BorderPane.setPkg("javafxs.scene.layout");
        BorderPane.setX(268);
        BorderPane.setY(718);
        BorderPane.setHeight(150);
        BorderPane.setWidth(150);
        
        manager.addClass(BorderPane);
        
        ClassObject FlowPane = new ClassObject(dummy, "class",true);
        FlowPane.setName("FlowPane");
        FlowPane.setPkg("javafx.scene.layout");
        FlowPane.setX(444);
        FlowPane.setY(718);
        FlowPane.setHeight(150);
        FlowPane.setWidth(150);
        
        manager.addClass(FlowPane);
        
        ClassObject Button = new ClassObject(dummy, "class",true);
        Button.setName("Button");
        Button.setPkg("javafx.scene.control");
        Button.setX(626);
        Button.setY(715);
        Button.setHeight(150);
        Button.setWidth(150);
        
        manager.addClass(Button);
        
        ClassObject ScrollPane = new ClassObject(dummy, "class",true);
        ScrollPane.setName("ScrollPane");
        ScrollPane.setPkg("javafx.scene.control");
        ScrollPane.setX(808);
        ScrollPane.setY(718);
        ScrollPane.setHeight(150);
        ScrollPane.setWidth(150);
        
        manager.addClass(ScrollPane);
        
        ClassObject TextArea = new ClassObject(dummy, "class",true);
        TextArea.setName("TextArea");
        TextArea.setPkg("javafx.scene.control");
        TextArea.setX(1003);
        TextArea.setY(709);
        TextArea.setHeight(150);
        TextArea.setWidth(150);
        
        manager.addClass(TextArea);
        
        ClassObject Platform = new ClassObject(dummy, "class",true);
        Platform.setName("Platform");
        Platform.setPkg("javafx.Application");
        Platform.setX(383);
        Platform.setY(288);
        Platform.setHeight(150);
        Platform.setWidth(150);
        
        manager.addClass(Platform);
        
        ClassObject Task = new ClassObject(dummy, "class",true);
        Task.setName("Task");
        Task.setPkg("javafx.concurrent");
        Task.setX(27);
        Task.setY(222);
        Task.setHeight(150);
        Task.setWidth(150);
        
        manager.addClass(Task);
        
        ClassObject Date = new ClassObject(dummy, "class",true);
        Date.setName("Date");
        Date.setPkg("java.util");
        Date.setX(37);
        Date.setY(447);
        Date.setHeight(150);
        Date.setWidth(150);
        
        manager.addClass(Date);
        
        ClassObject CounterTask = new ClassObject(dummy, "class",false);
        CounterTask.setName("CounterTask");
        CounterTask.setPkg("");
        CounterTask.setX(210);
        CounterTask.setY(120);
        CounterTask.setHeight(150);
        CounterTask.setWidth(150);

        var = new VariableObject();
        var.setAcccess("private");
        var.setStatic(false);
        var.setName("app");
        var.setType("ThreadExample");
        CounterTask.addVariable(var);

        var = new VariableObject();
        var.setAcccess("private");
        var.setStatic(false);
        var.setName("counter");
        var.setType("int");
        CounterTask.addVariable(var);
        
        method = new MethodObject();
        method.setName("CounterTask");
        method.setAccess("public");
        method.setStatic(false);
        method.setReturnType("constructor");
        var = new VariableObject();
        var.setAcccess("arg");
        var.setName("initApp");
        var.setStatic(false);
        var.setType("ThreadExample");
        method.addArg(var);
        CounterTask.addMethod(method);
        
        method = new MethodObject();
        method.setName("call");
        method.setAccess("public");
        method.setStatic(true);
        method.setReturnType("void");
        CounterTask.addMethod(method);
        
        manager.addClass(CounterTask);
        
        ClassObject DateTask = new ClassObject(dummy, "class",false);
        DateTask.setName("DateTask");
        DateTask.setPkg("");
        DateTask.setX(227);
        DateTask.setY(452);
        DateTask.setHeight(150);
        DateTask.setWidth(150);

        var = new VariableObject();
        var.setAcccess("private");
        var.setStatic(false);
        var.setName("app");
        var.setType("ThreadExample");
        DateTask.addVariable(var);

        var = new VariableObject();
        var.setAcccess("private");
        var.setStatic(false);
        var.setName("now");
        var.setType("Date");
        DateTask.addVariable(var);
        
        method = new MethodObject();
        method.setName("DateTask");
        method.setAccess("public");
        method.setStatic(false);
        method.setReturnType("constructor");
        var = new VariableObject();
        var.setAcccess("arg");
        var.setName("initApp");
        var.setStatic(false);
        var.setType("ThreadExample");
        method.addArg(var);
        DateTask.addMethod(method);
        
        method = new MethodObject();
        method.setName("call");
        method.setAccess("public");
        method.setStatic(true);
        method.setReturnType("void");
        DateTask.addMethod(method);
        
        manager.addClass(DateTask);
    }
}
