/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test_bed;

import java.util.ArrayList;
import javafx.scene.layout.Pane;
import jcd.JClassDesigner;
import jcd.controller.PageEditController;
import jcd.data.ClassObject;
import jcd.data.DataManager;
import jcd.data.MethodObject;
import jcd.data.VariableObject;
import jcd.file.FileManager;
import saf.components.AppComponentsBuilder;

/**
 *
 * @author rishab
 */
public class TestLoad 
{
    public static void main(String[] args) throws Exception 
    {
        DataManager manager;
        FileManager file;

        JClassDesigner designer = new JClassDesigner();
        AppComponentsBuilder builder = designer.makeAppBuilderHook();
        manager = (DataManager) builder.buildDataComponent();
        manager.setCanvas(new Pane());
        manager.setPageEditController(new PageEditController(designer));
        file = (FileManager) builder.buildFileComponent();

        file.loadData(manager, "work/DesignSaveTest.jcd");
        
         printData(manager.getClassObjects());
    }

    private static void printData(ArrayList<ClassObject> classObjects)
    {
        for(ClassObject obj: classObjects)
        {
            System.out.println("Class/Interface Name: " + obj.getName());
            ArrayList<VariableObject> instanceVars = obj.getVariables();
            for(int i = 0;i <instanceVars.size();i++)
            {
                System.out.println("Instance Variable " + (i+1) + ": " + instanceVars.get(i).getType()+ " " + instanceVars.get(i).getName());
            }
            ArrayList<MethodObject> methods = obj.getMethods();
            for(int i = 0;i < methods.size();i++)
            {
                MethodObject meth = methods.get(i);
                System.out.print(meth.getAccess() + " " + meth.getName() + "(");
                ArrayList<VariableObject> args = meth.getArgs();
                for(int n = 0;n <args.size();n++)
                {
                   if(n > 0)
                        System.out.print(", ");
                   System.out.print(args.get(n).getType());
                }
                System.out.println(")");
            }
            System.out.println("\n");
        }
    }
}
