/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd;


import java.util.Locale;
import javafx.stage.Stage;
import jcd.data.DataManager;
import jcd.file.FileManager;
import jcd.gui.ExternalClassDialogSingleton;
import jcd.gui.FullExternalClassDialogSingleton;
import jcd.gui.Workspace;
import saf.AppTemplate;
import saf.components.AppComponentsBuilder;
import saf.components.AppDataComponent;
import saf.components.AppFileComponent;
import saf.components.AppWorkspaceComponent;
/**
 *
 * @author rishab
 */
public class JClassDesigner extends AppTemplate
{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        Locale.setDefault(Locale.US);
        launch(args);
    }

    @Override
    public AppComponentsBuilder makeAppBuilderHook()
    {
        return new AppComponentsBuilder() 
        {
            public AppDataComponent buildDataComponent() throws Exception {
		return new DataManager(JClassDesigner.this);
	    }
            
            public AppFileComponent buildFileComponent() throws Exception {
		return new FileManager();
	    }
            
            public AppWorkspaceComponent buildWorkspaceComponent() throws Exception {
		return new Workspace(JClassDesigner.this);
	    }
    
        };
                
    }
    
    @Override
    public void start(Stage primaryStage)
    {
        ExternalClassDialogSingleton.getSingleton().init(primaryStage);
        FullExternalClassDialogSingleton.getSingleton().init(primaryStage);
        super.start(primaryStage);
    }
    
}
