/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.data;

import java.util.ArrayList;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;

/**
 *
 * @author rishab
 */
public class MethodObject
{
    private ArrayList<VariableObject> args;
    private SimpleStringProperty name;
    private SimpleStringProperty returnType;
    private SimpleStringProperty access;
    private SimpleBooleanProperty stat;
    private SimpleBooleanProperty abs;
    private String returnPackage;
    
    public MethodObject()
    {
        args = new ArrayList<>();
        returnType = new SimpleStringProperty();
        returnType.set("void");
        name = new SimpleStringProperty();
        name.set("");
        access = new SimpleStringProperty();
        access.set("public");
        stat = new SimpleBooleanProperty();
        stat.set(false);
        abs = new SimpleBooleanProperty();
        abs.set(false);
        returnPackage = "";
    }

    public String getName()
    {
        return name.getValue();
    }

    public String getAccess()
    {
        return access.getValue();
    }

    public boolean isStatic()
    {
        return stat.getValue(); 
    }

    public String getReturnType()
    {
        return returnType.getValue();
    }

    public ArrayList<VariableObject> getArgs()
    {
        return args;
    }

    public void setReturnType(String string)
    {
        returnType.setValue(string);
    }

    public void setStatic(boolean st) 
    {
        stat.setValue(st);
    }

    public void setAccess(String string) 
    {
        access.setValue(string);
    }

    public void setName(String string) 
    {
        name.setValue(string);
    }
    
    public void addArg(VariableObject var)
    {
        args.add(var);
    }
    
    public boolean isAbstract()
    {
        return abs.getValue();
    }
    
    public void setAbstract(boolean ab)
    {
        abs.setValue(ab);
    }

    public void setReturnPackage(String pack) 
    {  
        returnPackage = pack;
    }

    public void setArgs(ArrayList<VariableObject> vars)
    {
        args = vars;
    }

    public ObservableValue<String> getNameProperty() 
    {
        return name;
    }
    
    public String getReturnPackage()
    {
        return returnPackage;
    }
}
