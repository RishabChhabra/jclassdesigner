/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.data;

import java.util.ArrayList;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import jcd.controller.PageEditController;
import saf.AppTemplate;
import saf.components.AppDataComponent;

/**
 *
 * @author rishab
 */
public class DataManager implements AppDataComponent
{
    AppTemplate app;
    Pane canvas;
    ArrayList<ClassObject> classObjects;
    private PageEditController pageEditController;
    ArrayList<Line> grid;
    int maxX;
    int maxY;

    public DataManager(AppTemplate initApp)
    {
        app = initApp;
        classObjects = new ArrayList<ClassObject>();
        grid = new  ArrayList<>();
        maxX = 0;
        maxY = 0;
    }

    @Override
    public void reset()
    {
        classObjects.clear();
        if(canvas != null)
        {
            canvas.getChildren().clear();
//            if(!grid.isEmpty())
                canvas.getChildren().addAll(0,grid);
        }        
    }
    
    public Pane getCanvas()
    {
        return canvas;
    }
    
    public void setCanvas(Pane pane)
    {
        canvas = pane;
    }    
    
    public boolean isRepeat(ClassObject curr,String pack, String name)
    {
        for(ClassObject obj: classObjects)
        {
            if(obj != curr && pack.equals(obj.getPkg()) && name.equals(obj.getName()))
                return true;
        }
        return false;
    }
    
    public void addClass(ClassObject obj)
    {
        classObjects.add(obj);
    }
    
    public void removeClass(ClassObject obj)
    {
        classObjects.remove(obj);
    }

    public boolean hasOld(ClassObject curr, String[] oldInfo)
    {
        for(ClassObject obj: classObjects)
        {
            if(obj != curr && oldInfo[0].equals(obj.getPkg()) && oldInfo[1].equals(obj.getName()))
                return true;
        }
        return false;
    }
    
    public ArrayList<ClassObject> getClassObjects()
    {
        return classObjects;
    }
    
    public void  setPageEditController(PageEditController cont)
    {
        pageEditController = cont;
    }

    public PageEditController getPageEditController() 
    {
        return pageEditController;
    }
    
    public ClassObject getClass(String name,String pack)
    {
        if(name.equals("") && pack.equals(""))
            return null;
        
        for(ClassObject obj: classObjects)
        {
            if(obj.getPkg().equals(pack) && obj.getName().equals(name))
                return obj;
        }
        return null;
    }

    public void setGrid(boolean gr) 
    {
        if(gr)
        {
            grid = new ArrayList<>();
            for (int x = 0; x < canvas.getWidth(); x += 15) 
            {
                Line line = new Line();
                line.setStroke(Color.BLACK);
                line.setStartX(x);
                line.setEndX(x);
                line.setStartY(0);
                line.endYProperty().bind(canvas.heightProperty());
                grid.add(line);
                maxX = x;
//                    canvas.getChildren().add(0, line);
            }
            for (int y = 0; y < canvas.getHeight(); y += 15) 
            {
                Line line = new Line();
                line.setStroke(Color.BLACK);
                line.setStartX(0);
                line.endXProperty().bind(canvas.widthProperty());
                line.setStartY(y);
                line.setEndY(y);
                grid.add(line);
                maxY = y;
            }

            canvas.getChildren().addAll(0, grid);
        }
        else
        {
            canvas.getChildren().removeAll(grid);
            grid.clear();
        }
    }
    
    public void expandGridH()
    {
        for(int x = maxX + 15;x < canvas.widthProperty().doubleValue();x+=15)
        {
            Line line = new Line();
            line.setStroke(Color.BLACK);
            line.setStartX(x);
            line.setEndX(x);
            line.setStartY(0);
            line.endYProperty().bind(canvas.heightProperty());
            grid.add(line);
            canvas.getChildren().add(0,line);
            maxX = x;
        }
    }
    
    public void expandGridV()
    {
        for(int y = maxY + 15; y < canvas.heightProperty().doubleValue(); y+=15)
        {
            Line line = new Line();
            line.setStroke(Color.BLACK);
            line.setStartX(0);
            line.endXProperty().bind(canvas.widthProperty());
            line.setStartY(y);
            line.setEndY(y);
            grid.add(line);       
            canvas.getChildren().add(0,line);
            maxY = y;
        }
    }

    public boolean isPrimitive(String name, String pack)
    {
        return pack.equals("") && (name.equals("String") || name.equals("boolean") || name.equals("double") || name.equals("float") || name.equals("int") || name.equals("Object")
                || name.equals("constructor") || name.equals("void"));
    }
    
    public String extractClass(String name)
    {
        if(name.contains("["))
            name = name.substring(0,name.indexOf("["));
        if(name.contains("<"))
            name = name.substring(0,name.indexOf("<"));
        
        return name;
    }
}
