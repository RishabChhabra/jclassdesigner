/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.data;

import java.util.ArrayList;
import javafx.beans.property.DoubleProperty;
import javafx.scene.effect.Light.Point;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Polyline;
import javafx.scene.shape.Shape;
import javafx.scene.transform.Rotate;

/**
 *
 * @author rishab
 */
public class RelationshipLine
{
    ArrayList<Line> points;
    ArrayList<Line> subLines;
    Pane canvas;
    Shape connector;
    String type;
    
    public RelationshipLine(DoubleProperty startX,DoubleProperty startY, DoubleProperty endX, DoubleProperty endY,Pane pane,String typ)
    {
        
        canvas = pane;
        Line start = new Line();
        start.setStrokeWidth(5);
        start.startXProperty().bind(startX);
        start.endXProperty().bind(start.startXProperty());
        start.startYProperty().bind(startY);
        start.endYProperty().bind(start.startYProperty());
        
        Line end = new Line();
        end.setStrokeWidth(5);
        end.startXProperty().bind(endX);
        end.endXProperty().bind(end.startXProperty());
        end.startYProperty().bind(endY);
        end.endYProperty().bind(end.startYProperty());
        
        points = new ArrayList();
        points.add(start);
        points.add(end);
        
        Line line = new Line();
        line.startXProperty().bind(start.startXProperty());
        line.startXProperty().addListener(e ->{
            fixConnector();
        });
        line.startYProperty().bind(start.startYProperty());
        line.startYProperty().addListener(e ->{
            fixConnector();
        });
        line.endXProperty().bind(end.startXProperty());
        line.endXProperty().addListener(e ->{
            fixConnector();
        });
        line.endYProperty().bind(end.startYProperty());
        line.endYProperty().addListener(e ->{
            fixConnector();
        });
        line.setStroke(Color.BLACK);
        subLines = new ArrayList<>();
        subLines.add(line);     
        
        type = typ;
        
        if(type.equals("inheritance"))
        {
            connector = new Polygon(new double[]{endX.doubleValue(), endY.doubleValue(),
                endX.doubleValue() - 10, endY.doubleValue() + 10,
                endX.doubleValue() - 10, endY.doubleValue() - 10});
            connector.getTransforms().add(new Rotate(calculateBottomAngle(), endX.doubleValue(), endY.doubleValue()));
            canvas.getChildren().add(connector);
        }
        else if(type.equals("aggregation"))
        {
            connector = new Polygon(new double[]{startX.doubleValue(), startY.doubleValue(),
                startX.doubleValue() - 10, startY.doubleValue() + 10,
                startX.doubleValue() - 20, startY.doubleValue(),
                startX.doubleValue() - 10, startY.doubleValue() - 10});
            connector.getTransforms().add(new Rotate(calculateTopAngle(), startX.doubleValue(), startY.doubleValue()));
            canvas.getChildren().add(connector);
        }
        else if(type.equals("uses"))
        {
            connector = new Polyline(new double[]{startX.doubleValue()-10,startY.doubleValue()+7,
                startX.doubleValue(),startY.doubleValue(),
                startX.doubleValue()-10, startY.doubleValue()-7});
            connector.getTransforms().add(new Rotate(calculateTopAngle(), startX.doubleValue(), startY.doubleValue()));
            canvas.getChildren().add(connector);
        }
        
        
        canvas.getChildren().addAll(points);
        canvas.getChildren().addAll(subLines);

    }

    void removeSelf() 
    {
        for(Line point: points)
        {
            point.startXProperty().unbind();
            point.endYProperty().unbind();
        }
        canvas.getChildren().removeAll(points);
        
        for(Line line: subLines)
        {
            line.startXProperty().unbind();
            line.endXProperty().unbind();
            line.startYProperty().unbind();
            line.endYProperty().unbind();
        }
        canvas.getChildren().removeAll(subLines);
        canvas.getChildren().remove(connector);
    }

    private double calculateBottomAngle() 
    {
        Line line = subLines.get(0);
        double x = (line.getEndX() - line.getStartX());
        double y = (line.getEndY()-line.getStartY());
        double correction = 0;
        if((x < 0))
            correction = 180;
        return Math.toDegrees(Math.atan(y/x)) + correction;
    }

    private void fixConnector() 
    {
        
        Line line = subLines.get(0);
        canvas.getChildren().remove(connector);
        connector.getTransforms().clear();
        double x = 0;
        double y = 0;
        if(type.equals("inheritance"))
        {
            x = line.endXProperty().doubleValue();
            y= line.endYProperty().doubleValue();
            connector = new Polygon(new double[]{line.endXProperty().doubleValue(), line.endYProperty().doubleValue(),
                line.endXProperty().doubleValue() - 10, line.endYProperty().doubleValue() + 10,
                line.endXProperty().doubleValue() - 10, line.endYProperty().doubleValue() - 10});
            
            connector.getTransforms().add(new Rotate(calculateBottomAngle(), x, y));

        }
        else if (type.equals("aggregation"))
        {
            x = line.startXProperty().doubleValue();
            y = line.startYProperty().doubleValue();
            connector = new Polygon(new double[]{line.startXProperty().doubleValue(), line.startYProperty().doubleValue(),
                line.startXProperty().doubleValue() - 10, line.startYProperty().doubleValue() + 7,
                line.startXProperty().doubleValue() - 20, line.startYProperty().doubleValue(),
                line.startXProperty().doubleValue() - 10, line.startYProperty().doubleValue() - 7});
            
            connector.getTransforms().add(new Rotate(calculateTopAngle(), x, y));
        }
        else if(type.equals("uses"))
        {
            
            connector = new Polyline(new double[]{line.startXProperty().doubleValue()-10,line.startYProperty().doubleValue()+7,
                line.startXProperty().doubleValue(),line.startYProperty().doubleValue(),
                line.startXProperty().doubleValue()-10, line.startYProperty().doubleValue()-7});
            connector.getTransforms().add(new Rotate(calculateTopAngle(), line.startXProperty().doubleValue(), line.startYProperty().doubleValue()));
        }
        
//        connector.getTransforms().add(new Rotate(calculateBottomAngle(), x, y));
//        connector.setRotate(calculateAngle());
        
        canvas.getChildren().add(connector);
    }

    private double calculateTopAngle() 
    {
        Line line = subLines.get(0);
        double x = (line.getEndX() - line.getStartX());
        double y = (line.getEndY()-line.getStartY());
        double correction = 0;
        if((x > 0))
            correction = 180;
        return Math.toDegrees(Math.atan(y/x))+ correction;
    }

}
