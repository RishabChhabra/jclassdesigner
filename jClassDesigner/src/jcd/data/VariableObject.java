/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.data;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javax.json.JsonValue;
import sun.security.x509.CertificateSubjectName;

/**
 *
 * @author rishab
 */
public class VariableObject
{
    private String importPackage;
    private SimpleStringProperty type;
    private SimpleStringProperty name;
    private SimpleBooleanProperty stat;
    private SimpleStringProperty access;
    
    public VariableObject()
    {
        importPackage = "";
        type = new SimpleStringProperty();
        type.set("");
        name = new SimpleStringProperty();
        name.set("");
        stat = new SimpleBooleanProperty();
        stat.set(false);
        access = new SimpleStringProperty();
        access.set("");
    }
    
    public String getName()
    {
        return name.getValue();
    }    

    public String getType()
    {
        return type.getValue();
    }
    
    public String getAccess()
    {
        return access.getValue();
    }

    public boolean isStatic()
    {
        return stat.getValue();
    }

    public void setAcccess(String string) 
    {
        access.set(string);
    }

    public void setStatic(boolean st) 
    {
        stat.setValue(st);
    }

    public void setType(String string)
    {
        type.set(string);
    }

    public void setName(String string) 
    {
        name.setValue(string);
    }
    
    public SimpleStringProperty getNameProperty()
    {
        return name;
    }
    
    public SimpleStringProperty getTypeProperty()
    {
        return type;
    }
    
    public SimpleStringProperty getAccessProperty()
    {
        return access;
    }
    
    public BooleanProperty getStatProperty()
    {
        return stat;
    }

    public void setImportPackage(String pack) 
    {
        importPackage = pack;
    }

    public String getImportPackage()
    {
        return importPackage;
    }
}
