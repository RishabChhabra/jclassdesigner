/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.data;

import java.util.ArrayList;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;

/**
 *
 * @author rishab
 */
public class ClassObject extends Rectangle
{
    private StringProperty pkg;
    private StringProperty name;
    private ClassObject parent;
    private Label nameText;
    private Pane canvas; //knows the pane it is drawn on
    private ArrayList<MethodObject> methods;
    private ArrayList<VariableObject> variables;
    private String type;
    private boolean external;
    private ArrayList<ClassObject> interfaces;
    private ArrayList<Node> info;
    private Color color;
    private static boolean resizeable;
    private Line top;
    private Line bottom;
    private Line left;
    private Line right;
    private RelationshipLine parentLine;
    private ArrayList<RelationshipLine> implement;
    private ArrayList<RelationshipLine> uses;
    
    public ClassObject(Pane pane,String typ,boolean extern)
    {
        canvas = pane;
        type = typ;
        external = extern;
        pkg = new SimpleStringProperty();
        pkg.set("");
        name = new SimpleStringProperty();
        name.set("DefaultClass");
        nameText = new Label();
        resizeable = false;
        implement = new ArrayList<>();
        uses = new ArrayList<>();
        
        if(external)
        {
            color = Color.WHITE;
            setFill(Color.BLACK);
        }
        else
        {
            color = color.BLACK;
            setFill(Color.WHITE);
        }
        nameText.setTextFill(color);
        setStroke(Color.BLACK);
        setStrokeWidth(.5);
        if(type.equals("class"))
            nameText.textProperty().bind(name);
        else
            nameText.textProperty().bind(name.concat(" {" + type + "}"));
        
        nameText.maxWidthProperty().bind(this.widthProperty().subtract(10));
        nameText.layoutXProperty().bind(this.xProperty().add(5));
        nameText.layoutYProperty().bind(this.yProperty().add(5));
        nameText.setMouseTransparent(true);
        canvas.getChildren().addAll(this,nameText);
        
        methods = new ArrayList<>();
        variables = new ArrayList<>();
        interfaces = new ArrayList<>();
        info = new ArrayList();
        
        top = new Line();
        top.setStrokeWidth(4);
        top.setStroke(Color.TRANSPARENT);
        top.setStartX(getX());
        top.setEndX(getX()+ getWidth());
        top.startYProperty().set(getY());
        top.endYProperty().bind(top.startYProperty());
        top.setOnMouseEntered(e -> {
            if(resizeable)
                this.getScene().setCursor(Cursor.N_RESIZE);
        });
        top.setOnMouseExited(e -> {
            this.getScene().setCursor(Cursor.DEFAULT);
        });
        top.setOnMouseDragged(e -> {
            if(resizeable)
            {
                double y = e.getY();
                double height = bottom.getStartY() - y;
                if (height < 25)
                {
                    height = 25;
                    y = bottom.getStartY() - 25;
                }
                setHeight(height);
                setY(y);
                render();
            }
        });
        
        bottom = new Line();
        bottom.setStrokeWidth(4);
        bottom.setStroke(Color.TRANSPARENT);
        bottom.setStartX(getX());
        bottom.setEndX(getX()+ getWidth());
        bottom.startYProperty().set(getY()+getHeight());
        bottom.endYProperty().bind(bottom.startYProperty());
        bottom.setOnMouseEntered(e -> {
            if(resizeable)
                this.getScene().setCursor(Cursor.S_RESIZE);
        });
        bottom.setOnMouseExited(e -> {
            this.getScene().setCursor(Cursor.DEFAULT);
        });
        bottom.setOnMouseDragged(e -> {
            if(resizeable)
            {
                bottom.setStartY(e.getY());
                double height = bottom.getStartY() - top.getStartY();
                if (height < 25) {
                    height = 25;
                    bottom.setStartY(top.getStartY() + 25);
                }
                setHeight(height);
                render();
            }
        });
        
        right = new Line();
        right.setStrokeWidth(4);
        right.setStroke(Color.TRANSPARENT);
        right.setStartX(getX() + getWidth());
        right.endXProperty().bind(right.startXProperty());
        right.setStartY(getY());
        right.setEndY(getY() + getHeight());
        right.setOnMouseEntered(e -> {
            if(resizeable)
                this.getScene().setCursor(Cursor.E_RESIZE);
        });
        right.setOnMouseExited(e -> {
            this.getScene().setCursor(Cursor.DEFAULT);
        });
        right.setOnMouseDragged(e -> {
            if(resizeable)
            {
                right.setStartX(e.getX());
                double width = right.getStartX() - left.getStartX();
                if (width < 50) {
                    width = 50;
                    right.setStartX(left.getStartX() + 50);
                }
                setWidth(width);
                render();
            }
        });
        
        left = new Line();
        left.setStrokeWidth(4);
        left.setStroke(Color.TRANSPARENT);
        left.setStartX(getX());
        left.endXProperty().bind(left.startXProperty());
        left.setStartY(getY());
        left.setEndY(getY() + getHeight());
        left.setOnMouseEntered(e -> {
            if(resizeable)
                this.getScene().setCursor(Cursor.E_RESIZE);
        });
        left.setOnMouseExited(e -> {
            this.getScene().setCursor(Cursor.DEFAULT);
        });
        left.setOnMouseDragged(e -> {
            if(resizeable)
            {
                double x = e.getX();
                double width = right.getStartX() - x;
                if (width < 50) {
                    width = 50;
                    x = right.getStartX() - 50;
                }
                setWidth(width);
                setX(x);
                render();
            }
        });
        
        xProperty().addListener(e -> {
            fixEdges();
        });
        
        yProperty().addListener(e -> {
            fixEdges();
        });
        
        widthProperty().addListener(e -> {
            fixEdges();
        });
        
        heightProperty().addListener(e -> {
            fixEdges();
        });
        
        canvas.getChildren().add(canvas.getChildren().indexOf(this)+1,top);
        canvas.getChildren().add(canvas.getChildren().indexOf(this)+1,bottom);
        canvas.getChildren().add(canvas.getChildren().indexOf(this) + 1, right);
        canvas.getChildren().add(canvas.getChildren().indexOf(this) + 1, left);
    }
    
    public void setPkg(String str)
    {
        pkg.set(str);
        render();
    }
    
    public String getPkg()
    {
        return pkg.get();
    }
 
    public void setName(String str)
    {
        name.set(str);
        render();
    }
    
    public String getName()
    {
        return name.get();
    }

    public Property<String> getPackageProperty()
    {
        return pkg;
    }

    public Property<String> getClassNameProperty()
    {
        return name;
    }

    public void removeSelf()
    {
        if(parentLine != null)
            parentLine.removeSelf();
        for(RelationshipLine line: implement)
        {
            line.removeSelf();
        }
        for(RelationshipLine line: uses)
        {
            line.removeSelf();
        }
        canvas.getChildren().removeAll(this,nameText,top,bottom,left,right);
        canvas.getChildren().removeAll(info);
    }

    public String getType()
    {
        return type;
    }

    public ArrayList<VariableObject> getVariables()
    {
        return variables;
    }

    public ArrayList<MethodObject> getMethods()
    {
        return methods;
    }
    
    public void addVariable(VariableObject var)
    {
        variables.add(var);
        render();
    }
    
    public void addMethod(MethodObject method)
    {
        methods.add(method);
        render();
    }

    public ClassObject getParentClass()
    {
        return parent;
    }

    public void setParent(ClassObject par) 
    {
        parent = par;
        if(parent != null)
        {
            Line line = new Line();
            line.setStroke(Color.BLACK);
            line.startXProperty().bind(xProperty());
            line.startYProperty().bind(yProperty());
            line.endXProperty().bind(parent.xProperty());
            line.endYProperty().bind(parent.yProperty());
//            canvas.getChildren().add(0, line);
        }
        render();
    }
    
    public String toString()
    {
        if (!pkg.getValue().equals(""))
        {
            return pkg.getValue() + "." + name.getValue();
        }
        else 
        {
            return name.getValue();
        }
    }

    public void setExternal(boolean b) 
    {
        boolean external = b;
        
        render();
    }
    
    public boolean isExternal()
    {
        return external;
    }
    
    public ArrayList<ClassObject> getInterfaces()
    {
        return interfaces;
    }

    public void implementInterfaces(ArrayList<String> implement, DataManager data)
    {
        interfaces.clear();
        for(String str: implement)
        {
            String pack = "";
            String name = str;
            if (str.contains("."))
            {
                pack = str.substring(0, str.lastIndexOf("."));
                name = str.substring(str.lastIndexOf(".") + 1);
            }
            if(!name.equals(""))
            {
                interfaces.add(data.getClass(name, pack));
            }
        }
        render();
    }

    public void removeVariable(VariableObject variable)
    {
        variables.remove(variable);
        render();
    }

    public void removeMethod(MethodObject methodObject)
    {
        methods.remove(methodObject);
        render();
    }
    
    public void render()
    {
        canvas.getChildren().removeAll(info);
        info.clear();
        
        int offset = 20;
        if(offset < getHeight()-10)
        {
            Line divider = new Line();
            divider.startXProperty().bind(xProperty());
            divider.endXProperty().bind(xProperty().add(widthProperty()));
            divider.startYProperty().bind(nameText.layoutYProperty().add(offset));
            divider.endYProperty().bind(divider.startYProperty());
            divider.setStroke(color);
            divider.setMouseTransparent(true);
            info.add(divider);
            offset += 15;
        }
        
        int i = 0;
        while(offset < getHeight()-10 && i < variables.size())
        {
            VariableObject var = variables.get(i);
            Label label = new Label();
            SimpleStringProperty opener = new SimpleStringProperty("");
            
            if(var.getAccess().equals("public"))
                opener.set("+");
            else if(var.getAccess().equals("private"))
                opener.set("-");
            else
                opener.set("#");
            
            if(var.isStatic())
                opener.set(opener.getValue() + "$");
            
            label.setTextFill(color);
            label.textProperty().bind(opener.concat(var.getNameProperty().concat(":")).concat(var.getTypeProperty()));
            label.layoutXProperty().bind(xProperty().add(5));
            label.layoutYProperty().bind(yProperty().add(offset));
            label.maxWidthProperty().bind(widthProperty().subtract(10));
            label.setMouseTransparent(true);
            
            info.add(label);
            i++;
            offset+=15;
        }   
        
        if(offset < getHeight()-10)
        {
            Line divider = new Line();
            divider.startXProperty().bind(xProperty());
            divider.endXProperty().bind(xProperty().add(widthProperty()));
            divider.startYProperty().bind(nameText.layoutYProperty().add(offset));
            divider.endYProperty().bind(divider.startYProperty());
            divider.setStroke(color);
            divider.setMouseTransparent(true);
            info.add(divider);
            offset += 15;
        }
        
        i = 0;
        while(offset < getHeight()-10 && i < methods.size())
        {
            MethodObject method = methods.get(i);
            SimpleStringProperty display = new SimpleStringProperty();
            
            if(method.getAccess().equals("public"))
                display.set("+");
            else if(method.getAccess().equals("private"))
                display.set("-");
            else
                display.set("#");
            
            
            if(method.isStatic())
                display.set(display.getValue() + "$");
            
            display.set(display.getValue() + method.getName() + "(");
            
            ArrayList<VariableObject> args = method.getArgs();
            for(int j = 0; j < args.size();j++)
            {
                if(j > 0)
                    display.set(display.getValue() + ", " + args.get(j).getName() + ": " + args.get(j).getType());
                else
                    display.set(display.getValue() + args.get(j).getName() + ": " + args.get(j).getType());
            }
            
            display.set(display.getValue() + ")");
            
            if(!method.getReturnType().equals("constructor"))
                display.set(display.getValue() + ": " +  method.getReturnType());
            
            if(method.isAbstract())
                display.set(display.getValue() + " {abstract}");
            
            Label label = new Label();
            label.setTextFill(color);
            label.textProperty().bind(display);
            label.layoutXProperty().bind(xProperty().add(5));
            label.layoutYProperty().bind(yProperty().add(offset));
            label.maxWidthProperty().bind(widthProperty().subtract(10));
            label.setMouseTransparent(true);
            
            info.add(label);
            i++;
            offset+=15;
        }
        
        canvas.getChildren().addAll(canvas.getChildren().indexOf(this)+1,info);
    }    

    private void fixEdges() 
    {
        top.setStartX(getX());
        top.setEndX(getX() + getWidth());
        top.setStartY(getY());
        bottom.setStartX(getX());
        bottom.setEndX(getX() + getWidth());
        bottom.setStartY(getY() + getHeight());
        right.setStartX(getX() + getWidth());
        right.setStartY(getY());
        right.setEndY(getY() + getHeight());
        left.setStartX(getX());
        left.setEndY(getY() + getHeight());
        left.setStartY(getY());
    }

    public static void setResizeable(boolean b) 
    {
        resizeable = b;
    }
    
    public void renderRelationships(DataManager data)
    {
        if(parentLine != null)
            parentLine.removeSelf();
        if(parent != null)
        {   
            if(data.getClassObjects().contains(parent))
                parentLine = new RelationshipLine(xProperty(), yProperty(), parent.xProperty(), parent.yProperty(),canvas,"inheritance");
            else
                parent = null;
        }

        
        for(RelationshipLine line: implement)
        {
            line.removeSelf();
        }
        implement.clear();
        ArrayList<ClassObject> interfacesRemove = new ArrayList<>();
        for(ClassObject inter: interfaces)
        {
            if(data.getClassObjects().contains(inter))
                implement.add(new RelationshipLine(xProperty(), yProperty(), inter.xProperty(), inter.yProperty(),canvas,"inheritance"));
            else
                interfacesRemove.add(inter);
        }
        interfaces.removeAll(interfacesRemove);
        
        for(RelationshipLine line: uses)
        {
            line.removeSelf();
        }
        uses.clear();
        
        ArrayList<VariableObject> varsToRemove = new ArrayList<>();
        ArrayList<ClassObject> linesDrawn = new ArrayList<>();
        for(VariableObject var: variables)
        {
            String pack = var.getImportPackage();
            String type = data.extractClass(var.getType());
            
            if(!data.isPrimitive(type, pack))
            {
                ClassObject typeClass = data.getClass(type, pack);
                if(typeClass != null)
                {
                    if(!linesDrawn.contains(typeClass))
                    {
                        uses.add(new RelationshipLine(xProperty(), yProperty(), typeClass.xProperty(), typeClass.yProperty(), canvas,"aggregation"));
                        linesDrawn.add(typeClass);
                    }
                }
                else 
                {
                    if(!varsToRemove.contains(var))
                        varsToRemove.add(var);
                }
            }
        }
        variables.removeAll(varsToRemove);
        
        linesDrawn.clear();
        
        ArrayList<MethodObject> remove = new ArrayList<>();        
        for(MethodObject method: methods)
        {
            String pack = method.getReturnPackage();
            String type = data.extractClass(method.getReturnType());
            
            if(!data.isPrimitive(type, pack))
            {
                ClassObject returnClass = data.getClass(type, pack);
                if(returnClass != null)
                {
                    if(!linesDrawn.contains(returnClass))
                    {
                        uses.add(new RelationshipLine(xProperty(), yProperty(), returnClass.xProperty(), returnClass.yProperty(), canvas,"uses"));
                        linesDrawn.add(returnClass);
                    }
                }
                else 
                {
                    if(!remove.contains(method))
                        remove.add(method);
                }
            }
            
            ArrayList<VariableObject> varRemove = new ArrayList<>();
            for (VariableObject arg : method.getArgs()) 
            {
                String p = arg.getImportPackage();
                String t = data.extractClass(arg.getType());

                if (!data.isPrimitive(t, p))
                {
                    ClassObject obj = data.getClass(t, p);
                    if (obj != null)
                    {
                        if(!linesDrawn.contains(obj))
                        {
                            uses.add(new RelationshipLine(xProperty(), yProperty(), obj.xProperty(), obj.yProperty(), canvas,"uses"));
                            linesDrawn.add(obj);
                        }
                    } 
                    else 
                    {
                        if(!varRemove.contains(arg))
                            varRemove.add(arg);
                    }
                }

            }
            method.getArgs().removeAll(varRemove);
        }
        methods.removeAll(remove);
        render();
    }
}
