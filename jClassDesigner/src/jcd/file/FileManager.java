/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.file;

import java.io.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.SnapshotParameters;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javax.imageio.ImageIO;
import javax.json.*;
import javax.json.stream.JsonGenerator;
import jcd.PropertyType;
import static jcd.PropertyType.SNAP_FAILED_MESSAGE;
import static jcd.PropertyType.SNAP_FAILED_TITLE;
import static jcd.PropertyType.SNAP_SUCCESS_MESSAGE;
import static jcd.PropertyType.SNAP_SUCCESS_TITLE;
import jcd.data.ClassObject;
import jcd.data.DataManager;
import jcd.data.MethodObject;
import jcd.data.VariableObject;
import properties_manager.PropertiesManager;
import saf.AppTemplate;
import saf.components.AppDataComponent;
import saf.components.AppFileComponent;
import saf.ui.AppMessageDialogSingleton;

/**
 *
 * @author rishab
 */
public class FileManager implements AppFileComponent
{
    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException
    {
        StringWriter sw = new StringWriter();
        
        DataManager manager = (DataManager)data;
        
        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
        ArrayList<ClassObject> list =  manager.getClassObjects();
        fillArrayWithClasses(list,arrayBuilder);
        JsonArray classesArray = arrayBuilder.build();
        
        JsonObject dataJson;
        dataJson = Json.createObjectBuilder()
                .add("classes", classesArray)
                .build();
        
        Map<String, Object> props = new HashMap<>(1);
        props.put(JsonGenerator.PRETTY_PRINTING, true);
        JsonWriterFactory writerFactory = Json.createWriterFactory(props);
        JsonWriter jsonWriter = writerFactory.createWriter(sw);
        jsonWriter.writeObject(dataJson);
        jsonWriter.close();

        OutputStream out = new FileOutputStream(filePath);
        JsonWriter jsonFileWriter = Json.createWriter(out);
        jsonFileWriter.writeObject(dataJson);
        String prettyPrinted = sw.toString();
        PrintWriter pw = new PrintWriter(filePath);
        pw.write(prettyPrinted);
        pw.close();
    }
    
    private void fillArrayWithClasses(ArrayList<ClassObject> list, JsonArrayBuilder arrayBuilder)
    {
        Iterator iter = list.iterator();
        while(iter.hasNext())
        {
            ClassObject classObject = (ClassObject) iter.next();
            arrayBuilder.add(makeClassJsonObject(classObject));
        }
    }
    
    private JsonObject makeClassJsonObject(ClassObject classObject)
    {
        JsonObjectBuilder objBuilder = Json.createObjectBuilder();
        objBuilder.add("package", classObject.getPkg())
                .add("name", classObject.getName())
                .add("type", classObject.getType())
                .add("external", classObject.isExternal());
        if(classObject.getParentClass() != null)
                objBuilder.add("parent", classObject.getParentClass().toString());
        else
            objBuilder.add("parent", "");
        
        objBuilder.add("x", classObject.getX())
                .add("y", classObject.getY())
                .add("height", classObject.getHeight())
                .add("width", classObject.getWidth());
        
        ArrayList<ClassObject> interfaces = classObject.getInterfaces();
        JsonArrayBuilder interfacesArrayBuilder = Json.createArrayBuilder();
        fillArrayWithInterfaces(interfaces,interfacesArrayBuilder);
        objBuilder.add("interfaces", interfacesArrayBuilder.build());
        
        ArrayList<VariableObject> vars = classObject.getVariables();
        JsonArrayBuilder variableArrayBuilder = Json.createArrayBuilder();
        fillArrayWithVariables(vars, variableArrayBuilder);
        objBuilder.add("fields", variableArrayBuilder.build());
        
        ArrayList<MethodObject> methods = classObject.getMethods();
        JsonArrayBuilder methodArrayBuilder = Json.createArrayBuilder();
        fillArrayWithMethods(methods, methodArrayBuilder);
        objBuilder.add("methods", methodArrayBuilder.build());
        
        return objBuilder.build();
    }
    
    private void fillArrayWithInterfaces(ArrayList<ClassObject> interfaces, JsonArrayBuilder interfacesArrayBuilder)
    {
        Iterator iter = interfaces.iterator();
        while(iter.hasNext())
        {
            interfacesArrayBuilder.add(iter.next().toString());
        }
    }    
    
    private void fillArrayWithVariables(ArrayList<VariableObject> vars, JsonArrayBuilder variableArrayBuilder)
    {
        Iterator iter = vars.iterator();
        while(iter.hasNext())
        {
            VariableObject var = (VariableObject) iter.next();
            variableArrayBuilder.add(makeVariableJsonObject(var));
        }
    }
    
    private JsonObject makeVariableJsonObject(VariableObject var)
    {
        JsonObjectBuilder objectBuilder = Json.createObjectBuilder();
        objectBuilder.add("name",  var.getName())
                .add("type", var.getImportPackage() + "." + var.getType())
                .add("static", var.isStatic())
                .add("access", var.getAccess());
        
        return objectBuilder.build();
    }
    
    private void fillArrayWithMethods(ArrayList<MethodObject> methods, JsonArrayBuilder methodArrayBuilder)
    {
        Iterator iter = methods.iterator();
        while(iter.hasNext())
        {
            MethodObject method = (MethodObject) iter.next(); 
            methodArrayBuilder.add(makeMethodJsonObject(method));
        }
    }
    
    private JsonObject makeMethodJsonObject(MethodObject method)
    {
        JsonObjectBuilder objectBuilder = Json.createObjectBuilder();
        objectBuilder.add("name", method.getName())
                .add("access",method.getAccess())
                .add("static", method.isStatic())
                .add("return type", method.getReturnType())
                .add("abstract", method.isAbstract());
        
        ArrayList<VariableObject> args = method.getArgs();
        JsonArrayBuilder argsArrayBuilder = Json.createArrayBuilder();
        fillArrayWithVariables(args, argsArrayBuilder);
        objectBuilder.add("arguements", argsArrayBuilder.build());
        
        return objectBuilder.build();
    }
    
    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException
    {
        ArrayList<ClassObject> classes = ((DataManager)data).getClassObjects();
//        System.out.print(filePath);
        
        for(ClassObject classObject: classes)
        {
            if(!classObject.isExternal())
            {
                File dir = new File(filePath+"/" + classObject.getPkg());
                dir.mkdirs();
                File file= new File(dir.getAbsolutePath()+"/"+classObject.getName()+".java");
                
                generateJavaFile(file,data,classObject);
            }
        }
    }

    @Override
    public boolean loadData(AppDataComponent data, String filePath) throws IOException
    {
        JsonObject obj = loadJSONFile(filePath);
        
        JsonArray classes = obj.getJsonArray("classes");
        
        DataManager manager = (DataManager) data;
        manager.reset();
        
        //create Classes
        for(int i = 0;i < classes.size();i++)
        {
            makeClass((JsonObject)classes.get(i),manager);
        }
        
        //add parents
        for(int i = 0;i < classes.size();i++)
        {
            addParent(((JsonObject)classes.get(i)).getString("parent"),manager.getClassObjects().get(i),manager);
            
            ArrayList<String> list = new ArrayList();
            JsonArray jsonArray = ((JsonObject)classes.get(i)).getJsonArray("interfaces");
            for(int j = 0; j < jsonArray.size();j++)
            {
                list.add(jsonArray.getString(j));
            }
            manager.getClassObjects().get(i).implementInterfaces(list, manager);
        }
        
        for(int i = 0; i < classes.size();i++)
        {
            manager.getClassObjects().get(i).renderRelationships(manager);
        }
        return true;
    }
    
    private void makeClass(JsonObject jsonObject, DataManager data) 
    {
        ClassObject classObject = new ClassObject(data.getCanvas(), jsonObject.getString("type"),jsonObject.getBoolean("external"));
        classObject.setName(jsonObject.getString("name"));
        classObject.setPkg(jsonObject.getString("package"));
        classObject.setX(jsonObject.getJsonNumber("x").doubleValue());
        classObject.setY(jsonObject.getJsonNumber("y").doubleValue());
        classObject.setHeight(jsonObject.getJsonNumber("height").doubleValue());
        classObject.setWidth(jsonObject.getJsonNumber("width").doubleValue());
        data.addClass(classObject);
        
        JsonArray variables = jsonObject.getJsonArray("fields");
        loadVariables(variables,classObject); 
        
        JsonArray methods = jsonObject.getJsonArray("methods");
        loadMethods(methods,classObject);
        
        data.getPageEditController().addListeners(classObject);                
    }
    
    private void loadVariables(JsonArray variables, ClassObject classObject)
    {
        for(int i = 0;i < variables.size();i++)
        {
            VariableObject var = makeVariable((JsonObject)variables.get(i));
            classObject.addVariable(var);
        }
    }

    private void loadMethods(JsonArray methods, ClassObject classObject) 
    {
        for (int i = 0; i < methods.size(); i++)        
        {
            MethodObject method = makeMethod((JsonObject)methods.get(i));
            classObject.addMethod(method);
        }   
    }
    
    private VariableObject makeVariable(JsonObject jsonObject) 
    {
        VariableObject var = new VariableObject();
        var.setName(jsonObject.getString("name"));
        var.setImportPackage(jsonObject.getString("type").substring(0,jsonObject.getString("type").lastIndexOf(".")));
        var.setType(jsonObject.getString("type").substring(jsonObject.getString("type").lastIndexOf(".")+1));
        var.setStatic(jsonObject.getBoolean("static"));
        var.setAcccess(jsonObject.getString("access"));
        
        return var;
    }
    
    private MethodObject makeMethod(JsonObject jsonObject) 
    {
        MethodObject method = new MethodObject();
        method.setName(jsonObject.getString("name"));
        method.setAccess(jsonObject.getString("access"));
        method.setStatic(jsonObject.getBoolean("static"));
        method.setReturnType(jsonObject.getString("return type"));
        method.setAbstract(jsonObject.getBoolean("abstract"));
        
        JsonArray args = jsonObject.getJsonArray("arguements");
        loadArguements(args,method);
        
        return method;
    }
    
    private void loadArguements(JsonArray args, MethodObject method) 
    {
        for(int i = 0;i < args.size();i++)
        {
            VariableObject var = makeVariable((JsonObject)args.get(i));
            method.addArg(var);
        }    
    }
    
    private void addParent(String str,ClassObject classObject, DataManager manager)
    {
        if(str.equals(""))
            classObject.setParent(null);
        else 
        {
            String pack = "";
            String name = str;
            if (str.contains(".")) 
            {
                pack = str.substring(0, str.lastIndexOf("."));
                name = str.substring(str.lastIndexOf(".") + 1);
            }
            if (name.equals("")) 
                classObject.setParent(null);
            else 
            {
                ClassObject parent = manager.getClass(name, pack);
                
                classObject.setParent(parent);
       
            }
        }            
    }
    
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException
    {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }

    @Override
    public void takeSnapshot(AppDataComponent data, AppTemplate app)
    {
         Pane canvas = ((DataManager)data).getCanvas();
            PropertiesManager propManager = PropertiesManager.getPropertiesManager();

            if(canvas != null)
            {
                WritableImage image = (canvas.snapshot(new SnapshotParameters(), null));
                File dir = new File(propManager.getProperty(PropertyType.PATH_SNAPSHOTS));
                dir.mkdir();
                FileChooser fc = new FileChooser();
                fc.setInitialDirectory(new File(propManager.getProperty(PropertyType.PATH_SNAPSHOTS)));
                fc.setTitle(propManager.getProperty(PropertyType.SNAP_LOCATION_TITLE));
                fc.getExtensionFilters().addAll(
                        new FileChooser.ExtensionFilter("images", "*.png"));
                File selectedFile = fc.showSaveDialog(app.getGUI().getWindow());
                if (selectedFile != null) 
                {
                    try
                    {
                        ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", selectedFile);
                        AppMessageDialogSingleton msg = AppMessageDialogSingleton.getSingleton();
                        msg.show(propManager.getProperty(SNAP_SUCCESS_TITLE), propManager.getProperty(SNAP_SUCCESS_MESSAGE));
                    } catch (IOException ex)
                    {
                        AppMessageDialogSingleton err = AppMessageDialogSingleton.getSingleton();
                        err.show(propManager.getProperty(SNAP_FAILED_TITLE), propManager.getProperty(SNAP_FAILED_MESSAGE.toString()));
                    }
                }
            }
            else
            {
                AppMessageDialogSingleton err = AppMessageDialogSingleton.getSingleton();
                err.show(propManager.getProperty(SNAP_FAILED_TITLE), propManager.getProperty(SNAP_FAILED_MESSAGE.toString()));
            }
    }

    private void generateJavaFile(File file, AppDataComponent data, ClassObject classObject)
    {
        try 
        {
            PrintWriter writer = new  PrintWriter(file);
            if(!classObject.getPkg().equals(""))
                writer.println("package " + classObject.getPkg() + ";\n");
                
            writer.println(generateImports(data,classObject));
            
            if(classObject.getType().equals("interface"))
                writer.println("public interface " + classObject.getName() + interfaceHeader(classObject) + "\n{");
            else
            {
                if(classObject.getType().equals("abstract"))
                    writer.println("public abstract class " + classObject.getName() + parentHeader(classObject) + "\n{");
                else
                    writer.println("public class " + classObject.getName() + parentHeader(classObject) + "\n{");            
            }
            
            writer.println(generateVariables(classObject));
            
            writer.print(generateMethods(classObject));
            
            writer.print("}");
                    
            writer.close();
        } 
        catch (FileNotFoundException ex) 
        {
            Logger.getLogger(FileManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    private String generateImports(AppDataComponent data, ClassObject classObject) 
    {
        DataManager dataManager = ((DataManager)data);
        
        String str = "";
        
        ArrayList<VariableObject> fields = classObject.getVariables();
        for(VariableObject field: fields)
        {
            String pack = field.getImportPackage();
            String type = dataManager.extractClass(field.getType());
            
            if(!pack.equals(classObject.getPkg()) && !dataManager.isPrimitive(type, pack))
            {
                if(!str.contains("import " + pack + "." + type + ";\n"))
                    str += "import " + pack + "." + type + ";\n";
            }            
        }
        
        ArrayList<MethodObject> methods = classObject.getMethods();
        for(MethodObject method:methods)
        {
            String returnPackage = method.getReturnPackage();
            String returnType = dataManager.extractClass(method.getReturnType());
            
            if(!returnPackage.equals(classObject.getPkg()) && !dataManager.isPrimitive(returnType, returnPackage))
            {
                if(!str.contains("import " + returnPackage + "." + returnType + ";\n"))
                    str += "import " + returnPackage + "." + returnType + ";\n";
            }
            
            ArrayList<VariableObject> args = method.getArgs();
            for(VariableObject arg: args)
            {
                String pack = arg.getImportPackage();
                String type = dataManager.extractClass(arg.getType());
                
                if(!pack.equals(classObject.getPkg()) && !dataManager.isPrimitive(type, pack))
                {
                    if(!str.contains("import " + pack + "." + type + ";\n"))
                        str += "import " + pack + "." + type + ";\n";
                }                 
            }            
        }        
        
        if(classObject.getParentClass() != null)
        {
            String parentPackage = classObject.getParentClass().getPkg();
            String parentName = dataManager.extractClass(classObject.getParentClass().getName());

            if (!parentPackage.equals(classObject.getPkg()) && !dataManager.isPrimitive(parentName, parentPackage))
            {
                if(!str.contains("import " + parentPackage + "." + parentName + ";\n"))
                    str += "import " + parentPackage + "." + parentName + ";\n";
            }
        }
        
        ArrayList<ClassObject> interfaces = classObject.getInterfaces();
        for(ClassObject inter: interfaces)
        {
            String parentPackage = inter.getPkg();
            String parentName = dataManager.extractClass(inter.getName());

            if (!parentPackage.equals(classObject.getPkg()) && !dataManager.isPrimitive(parentName, parentPackage))
            {
                if(!str.contains("import " + parentPackage + "." + parentName + ";\n"))
                    str += "import " + parentPackage + "." + parentName + ";\n";
            }
        }
        return str;
    }

    private String parentHeader(ClassObject classObject) 
    {
        String str = "";
        
        if(classObject.getParentClass() != null)
        {
            str +=  " extends " + classObject.getParentClass().getName();
        }
        
        ArrayList<ClassObject> interfaces = classObject.getInterfaces();
        for(int i = 0;i < interfaces.size();i++)
        {
            ClassObject inter = interfaces.get(i);
            if(i == 0)
            {
                str += " implements " + inter.getName();
            }
            else
            {
                str += ", " + inter.getName();
            }
        }
        
        return str;
    }

    private String generateVariables(ClassObject classObject) 
    {
        String str = "";
        
        ArrayList<VariableObject> vars= classObject.getVariables();
        for(VariableObject var: vars)
        {
            if(var.isStatic())
                str += "\t" + var.getAccess() + " static " + var.getType() + " " + var.getName() + ";\n";
            else
                str += "\t" + var.getAccess() + " " + var.getType() + " " + var.getName() + ";\n";
        }
        
        return str;
    }

    private String interfaceHeader(ClassObject classObject) 
    {
        String str = "";
        ArrayList<ClassObject> interfaces = classObject.getInterfaces();
        for(int i = 0;i < interfaces.size();i++)
        {
            ClassObject inter = interfaces.get(i);
            if(i == 0)
            {
                str += " extends " + inter.getName();
            }
            else
            {
                str += ", " + inter.getName();
            }
        }
        return str;
    }

    private String generateMethods(ClassObject classObject) 
    {
        String str = "";
        
        ArrayList<MethodObject> methods = classObject.getMethods();
        for(MethodObject method: methods)
        {
            str += "\n" + generateMethod(method) + "\n";
        }
        
        return str;
    }

    private String generateMethod(MethodObject method) 
    {
        String str = "\t" +  method.getAccess() + " ";
        
        if(method.isAbstract())
        {
            str += "abstract "; 
            
            if(method.isStatic())
                str += "static ";
            if(method.getReturnType().equals("constructor"))
                str += method.getName() + "(" + generateArgs(method) + ");";
            else
                str += method.getReturnType() + " " + method.getName() + "(" + generateArgs(method) + ");";
        }
        else
        {
            if(method.isStatic())
                str += "static "; 
            if(method.getReturnType().equals("constructor"))
                str += method.getName() + "(" + generateArgs(method) + ")\n\t{\n\t\tthrow new UnsupportedOperationException(\"Not supported yet.\");\n\t}";
            else
                str += method.getReturnType() + " " + method.getName() + "(" + generateArgs(method) + ")\n\t{\n\t\tthrow new UnsupportedOperationException(\"Not supported yet.\");\n\t}";

        }
        
        
        
        return str;
    }

    private String generateArgs(MethodObject method) 
    {
        String str = "";
        
        ArrayList<VariableObject> args = method.getArgs();
        for(int i = 0; i < args.size();i++)
        {
            VariableObject arg = args.get(i);
            if(i == 0)
                str += arg.getType() + " " + arg.getName();
            else
                str += ", " + arg.getType() + " " + arg.getName();
        }
        return str;
    }
}