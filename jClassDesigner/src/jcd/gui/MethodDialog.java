/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.gui;

import com.sun.deploy.uitoolkit.impl.fx.ui.FXAboutDialog;
import java.util.ArrayList;
import java.util.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.TextFlow;
import javafx.stage.Modality;
import javafx.stage.Stage;
import jcd.data.ClassObject;
import jcd.data.MethodObject;
import jcd.data.VariableObject;

/**
 *
 * @author rishab
 */
public class MethodDialog  extends Stage
{
    VBox messagePane;
    Scene messageScene;
    Button okButton;
    Button cancelButton;
    String selection;
    ComboBox<String> typeComboBox;
    TextField nameTextField;
    CheckBox staticCheckBox;
    CheckBox abstractCheckBox;
    ComboBox<String> accessComboBox;
    ArrayList<Button> removeButtons;
    ArrayList<VBox> argBoxes;
    VBox argsBox;
    ObservableList classes;
    
    // CONSTANT CHOICES
    public static final String OK = "Okay";
    public static final String CANCEL = "Cancel";
    
    public MethodDialog() {}

    public MethodDialog(Stage primaryStage) 
    {
        init(primaryStage);
    }

	

    public void init(Stage primaryStage) {
        // MAKE THIS DIALOG MODAL, MEANING OTHERS WILL WAIT
        // FOR IT WHEN IT IS DISPLAYED
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        
        argBoxes = new ArrayList();
        removeButtons = new ArrayList();
        
        selection = "";
                
        // YES, NO, AND CANCEL BUTTONS
        okButton = new Button(OK);
        cancelButton = new Button(CANCEL);
	// MAKE THE EVENT HANDLER FOR THESE BUTTONS
        EventHandler okHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            Button sourceButton = (Button)ae.getSource();
            MethodDialog.this.selection = sourceButton.getText();
            MethodDialog.this.hide();
        };
        
	// AND THEN REGISTER THEM TO RESPOND TO INTERACTIONS
        okButton.setOnAction(okHandler);
        cancelButton.setOnAction(okHandler);

        // NOW ORGANIZE OUR BUTTONS
        HBox buttonBox = new HBox(5);
        buttonBox.getChildren().addAll(okButton,cancelButton);
        
        
        
        
        Label typeLabel = new Label("Return Type:");
        typeComboBox = new ComboBox<>();
        typeComboBox.setPromptText("package.class");
        typeComboBox.setEditable(true);
        HBox typeBox = new HBox(5);
        typeBox.getChildren().addAll(typeLabel,typeComboBox);
        
        Label nameLabel = new Label("Name:");
        nameTextField = new TextField();
        HBox nameBox = new HBox(5);
        nameBox.getChildren().addAll(nameLabel,nameTextField);
        
        Label staticLabel = new Label("Static:");
        staticCheckBox = new CheckBox();
        HBox staticBox = new HBox(5);
        staticBox.getChildren().addAll(staticLabel,staticCheckBox);
        
        
        Label accessLabel = new Label("Access:");
        accessComboBox = new ComboBox<>();
        accessComboBox.setEditable(false);
        accessComboBox.getItems().addAll("public","private","protected");
        accessComboBox.getSelectionModel().select(0);
        HBox accessBox = new HBox(5);
        accessBox.getChildren().addAll(accessLabel,accessComboBox);
        
        Label abstractLabel = new Label("Abstract:");
        abstractCheckBox = new CheckBox();
        HBox abstractBox = new HBox(5);
        abstractBox.getChildren().addAll(abstractLabel,abstractCheckBox);
        
        HBox togglesBox = new HBox(15);
        togglesBox.getChildren().add(staticBox);
        togglesBox.getChildren().add(abstractBox);
        
        HBox argHeader = new HBox(5);
        Label argLabel = new Label("Arguments: ");
        Button addArgButton = new Button();
        addArgButton.setTooltip(new Tooltip("Add Argument"));
        addArgButton.setScaleX(.75);
        addArgButton.setScaleY(.75);
        addArgButton.setGraphic(new ImageView(new Image("file:./images/Add.png")));        
        addArgButton.setOnAction(e ->{

            Label type = new Label("Arg Type:");
            ComboBox types = new ComboBox(classes);
            types.setPromptText("package.class");
            types.setEditable(true);
            HBox argType = new HBox(5);
            argType.getChildren().addAll(type,types);
            
            Label argName = new Label("Arg Name:");
            TextField name = new TextField();
            HBox argNameBox = new HBox(5);
            Button removeArgButton = new Button();
            removeArgButton.setTooltip(new Tooltip("Remove Argument"));
            removeArgButton.setScaleX(.75);
            removeArgButton.setScaleY(.75);
            removeArgButton.setGraphic(new ImageView(new Image("file:./images/Remove.png")));
            removeButtons.add(removeArgButton);
            argNameBox.getChildren().addAll(argName,name,removeArgButton);
            
            VBox box = new VBox(10); 
            box.setStyle("-fx-padding: 10;" + 
                      "-fx-border-style: solid inside;" + 
                      "-fx-border-width: 2;" +
                      "-fx-border-insets: 5;" + 
                      "-fx-border-radius: 5;" + 
                      "-fx-border-color: blue;");
            box.getChildren().addAll(argType,argNameBox);
            argsBox.getChildren().add(box);
            argBoxes.add(box);
            sizeToScene();
            
            removeArgButton.setOnAction(ea -> {
                int loc = removeButtons.indexOf(ea.getSource());
                removeButtons.remove(loc);
                argsBox.getChildren().remove(argBoxes.get(loc));
                argBoxes.remove(loc);
                sizeToScene();
            });
        });
        argHeader.getChildren().addAll(argLabel,addArgButton);
        
        argsBox = new VBox(10);
       
        // WE'LL PUT EVERYTHING HERE
        messagePane = new VBox();
        messagePane.setAlignment(Pos.CENTER_LEFT);                
        messagePane.getChildren().addAll(nameBox,accessBox,typeBox,togglesBox,argHeader,argsBox,buttonBox);        
        // MAKE IT LOOK NICE
        messagePane.setPadding(new Insets(10, 20, 20, 20));
        messagePane.setSpacing(10);

        // AND PUT IT IN THE WINDOW
        messageScene = new Scene(messagePane);
        this.setScene(messageScene);
    }

    /**
     * Accessor method for getting the selection the user made.
     * 
     * @return Either YES or NO, depending on which
     * button the user selected when this dialog was presented.
     */
    public String getSelection() 
    {
        return selection;
    }
    
    public String getName()
    {
        return  nameTextField.getText().trim();
    }
    
    public boolean getStatic()
    {
        return staticCheckBox.isSelected();
    }
    
    public String getAccess()
    {
        return accessComboBox.getSelectionModel().getSelectedItem().trim();
    }
 
    public String getReturnType() 
    {
        return typeComboBox.getSelectionModel().getSelectedItem().trim();
    }

    public boolean getAbstract() 
    {        
        return abstractCheckBox.isSelected();
    }
    
    public ArrayList<String[]> getArguments()
    {
        ArrayList<String[]> args = new ArrayList();
        for(int i = 0;i < argBoxes.size();i++)
        {
            String name = "" + ((TextField)((HBox)argBoxes.get(i).getChildren().get(1)).getChildren().get(1)).getText();
//            if(name !=  null)
//                name = name.trim();
            String type = "" + ((ComboBox<String>)((HBox)argBoxes.get(i).getChildren().get(0)).getChildren().get(1)).getSelectionModel().getSelectedItem();
//            if(type != null)
//                type = type.trim();
            
            if(!type.equals("") && !name.equals(""))
            {
                String[] data = {type,name};
                args.add(data);
            }            
        }
        return args;
    }
    
    /**
     * This method loads a custom message into the label
     * then pops open the dialog.
     * 
     * @param title The title to appear in the dialog window bar.
     * 
     * @param message Message to appear inside the dialog.
     */
    public void show(ArrayList<ClassObject> all, boolean isInterface)
    {
        setTitle("New Method");
        
        classes = FXCollections.observableArrayList();
        for(ClassObject obj: all)
        {
            classes.add(obj.toString());
        }
        typeComboBox.getItems().add("void");
        typeComboBox.getItems().addAll(classes);
        typeComboBox.getSelectionModel().select(0);

        abstractCheckBox.setSelected(isInterface);
        abstractCheckBox.setDisable(isInterface);
        
        
        
        showAndWait();
    }    

    public void show(ArrayList<ClassObject> all, MethodObject item,boolean isInterface) 
    {
        setTitle("Edit Method");
        
        classes = FXCollections.observableArrayList();
        for(ClassObject obj: all)
        {
            classes.add(obj.toString());
        }
        typeComboBox.getItems().add("void");
        typeComboBox.getItems().addAll(classes);
        
        String fullReturn = item.getReturnType();
        if(!item.getReturnPackage().equals(""))
            fullReturn = item.getReturnPackage() + "." + item.getReturnType();
        typeComboBox.getSelectionModel().select(fullReturn);
        
        nameTextField.setText(item.getName());
        abstractCheckBox.setSelected(item.isAbstract());
        staticCheckBox.setSelected(item.isStatic());
        
        ArrayList<VariableObject> args = item.getArgs();
        for(VariableObject var: args)
        {
            String fullVar = var.getType();
            if(!var.getImportPackage().equals(""))
                fullVar = var.getImportPackage() + "." + var.getType();
            Label type = new Label("Arg Type:");
            ComboBox types = new ComboBox(classes);
            types.setPromptText("package.class");
            types.setEditable(true);
            types.getSelectionModel().select(fullVar);
            HBox argType = new HBox(5);
            argType.getChildren().addAll(type,types);
            
            
            Label argName = new Label("Arg Name:");
            TextField name = new TextField();
            name.setText(var.getName());
            HBox argNameBox = new HBox(5);
            Button removeArgButton = new Button();
            removeArgButton.setTooltip(new Tooltip("Remove Argument"));
            removeArgButton.setScaleX(.75);
            removeArgButton.setScaleY(.75);
            removeArgButton.setGraphic(new ImageView(new Image("file:./images/Remove.png")));
            removeButtons.add(removeArgButton);
            argNameBox.getChildren().addAll(argName,name,removeArgButton);
            
            VBox box = new VBox(10); 
            box.setStyle("-fx-padding: 10;" + 
                      "-fx-border-style: solid inside;" + 
                      "-fx-border-width: 2;" +
                      "-fx-border-insets: 5;" + 
                      "-fx-border-radius: 5;" + 
                      "-fx-border-color: blue;");
            box.getChildren().addAll(argType,argNameBox);
            argsBox.getChildren().add(box);
            argBoxes.add(box);
            sizeToScene();
            
            removeArgButton.setOnAction(ea -> {
                int loc = removeButtons.indexOf(ea.getSource());
                removeButtons.remove(loc);
                argsBox.getChildren().remove(argBoxes.get(loc));
                argBoxes.remove(loc);
                sizeToScene();
            });
        }
        
        abstractCheckBox.setSelected(isInterface);
        abstractCheckBox.setDisable(isInterface);
        showAndWait();
    }    
}