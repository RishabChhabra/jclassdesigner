/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.gui;

import java.util.ArrayList;
import java.util.List;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import jcd.data.ClassObject;
import org.controlsfx.control.CheckComboBox;

/**
 *
 * @author rishab
 */
public class InterfaceDialog extends Stage
{
    
    // GUI CONTROLS FOR OUR DIALOG
    VBox messagePane;
    Scene messageScene;
    Button okButton;
    String selection;
    CheckComboBox<String> interfaces;
    TextField externalTextField;
    
    // CONSTANT CHOICES
    public static final String OK = "Okay";
    
    public InterfaceDialog() {}

    public InterfaceDialog(Stage primaryStage) 
    {
        init(primaryStage);
    }

	

    public void init(Stage primaryStage) {
        // MAKE THIS DIALOG MODAL, MEANING OTHERS WILL WAIT
        // FOR IT WHEN IT IS DISPLAYED
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
                
        // YES, NO, AND CANCEL BUTTONS
        okButton = new Button(OK);	
	// MAKE THE EVENT HANDLER FOR THESE BUTTONS
        EventHandler okHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            Button sourceButton = (Button)ae.getSource();
            InterfaceDialog.this.selection = sourceButton.getText();
            InterfaceDialog.this.hide();
        };
        
	// AND THEN REGISTER THEM TO RESPOND TO INTERACTIONS
        okButton.setOnAction(okHandler);

        // NOW ORGANIZE OUR BUTTONS
        HBox buttonBox = new HBox();
        buttonBox.getChildren().add(okButton);
        
        HBox existent = new HBox();
        Label existentLabel = new Label("Implement interface(s):");
        interfaces = new CheckComboBox();
        interfaces.setMaxWidth(130);
        existent.getChildren().addAll(existentLabel,interfaces);
        
        HBox external = new HBox();
        Label externalLabel = new Label("External Interface:");
        externalTextField = new TextField();
        externalTextField.setPromptText("package.interface");
        externalTextField.setOnKeyPressed(e -> {
            if(e.getCode().equals(KeyCode.ENTER))
            {
                if(!externalTextField.getText().equals(""))
                {
                    interfaces.getItems().add(externalTextField.getText());
                    List list = interfaces.getCheckModel().getCheckedIndices();
                    int[] nums = new int[list.size()+1];
                    for(int i = 0;i < nums.length;i++)
                    {
                        nums[i] = (int) list.get(i);
                    }
                    nums[nums.length-1] = interfaces.getItems().size()-1;
                    interfaces.getCheckModel().checkIndices(nums);
                    externalTextField.setText("");
                }
            }
        });
        external.getChildren().addAll(externalLabel,externalTextField);
        
        
        // WE'LL PUT EVERYTHING HERE
        messagePane = new VBox();
        messagePane.setAlignment(Pos.CENTER_LEFT);                
        messagePane.getChildren().addAll(existent,external,buttonBox);        
        // MAKE IT LOOK NICE
        messagePane.setPadding(new Insets(10, 20, 20, 20));
        messagePane.setSpacing(10);

        // AND PUT IT IN THE WINDOW
        messageScene = new Scene(messagePane);
        this.setScene(messageScene);
    }

    /**
     * Accessor method for getting the selection the user made.
     * 
     * @return Either YES or NO, depending on which
     * button the user selected when this dialog was presented.
     */
    public String getSelection() 
    {
        return selection;
    }
    
    public ArrayList<String> getInterfaces()
    {
        ArrayList<String> list = new ArrayList<>();
        
        for(String str: interfaces.getCheckModel().getCheckedItems())
        {
            list.add(str.trim());
        }            
        return list;
    }

 
    /**
     * This method loads a custom message into the label
     * then pops open the dialog.
     * 
     * @param title The title to appear in the dialog window bar.
     * 
     * @param message Message to appear inside the dialog.
     */
    public void show(ClassObject curr, ArrayList<ClassObject> all)
    {
	setTitle(curr.toString());
	for(ClassObject obj: all)
        {
            if(obj != curr && obj.getType().equals("interface"))
                interfaces.getItems().add(obj.toString());
        }
        int[] select = new int[curr.getInterfaces().size()];
        for(int i = 0; i < select.length;i++)
        {
            select[i] = interfaces.getCheckModel().getItemIndex(curr.getInterfaces().get(i).toString());
        }
        interfaces.getCheckModel().checkIndices(select);
        showAndWait();
    }
}
