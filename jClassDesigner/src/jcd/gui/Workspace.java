/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.gui;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.*;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.util.Callback;
import static jcd.PropertyType.ADD_ARG_ICON;
import static jcd.PropertyType.ADD_ARG_TOOLTIP;
import static jcd.PropertyType.ADD_CLASS_ICON;
import static jcd.PropertyType.ADD_CLASS_TOOLTIP;
import static jcd.PropertyType.ADD_ICON;
import static jcd.PropertyType.ADD_INTERFACE_ICON;
import static jcd.PropertyType.ADD_INTERFACE_TOOLTIP;
import static jcd.PropertyType.ADD_METHOD_TOOLTIP;
import static jcd.PropertyType.ADD_VARIABLE_TOOLTIP;
import static jcd.PropertyType.CLASS_LABEL;
import static jcd.PropertyType.GRID_LABEL;
import static jcd.PropertyType.METHOD_LABEL;
import static jcd.PropertyType.PACKAGE_LABEL;
import static jcd.PropertyType.PARENT_LABEL;
import static jcd.PropertyType.REDO_ICON;
import static jcd.PropertyType.REDO_TOOLTIP;
import static jcd.PropertyType.REMOVE_ARG_ICON;
import static jcd.PropertyType.REMOVE_ARG_TOOLTIP;
import static jcd.PropertyType.REMOVE_ICON;
import static jcd.PropertyType.REMOVE_METHOD_TOOLTIP;
import static jcd.PropertyType.REMOVE_SIDE_ICON;
import static jcd.PropertyType.REMOVE_TOOLTIP;
import static jcd.PropertyType.REMOVE_VARIABLE_TOOLTIP;
import static jcd.PropertyType.RESIZE_ICON;
import static jcd.PropertyType.RESIZE_TOOLTIP;
import static jcd.PropertyType.SELECT_ICON;
import static jcd.PropertyType.SELECT_TOOLTIP;
import static jcd.PropertyType.SNAP_LABEL;
import static jcd.PropertyType.UNDO_ICON;
import static jcd.PropertyType.UNDO_TOOLTIP;
import static jcd.PropertyType.VARIABLE_LABEL;
import static jcd.PropertyType.ZOOM_IN_ICON;
import static jcd.PropertyType.ZOOM_IN_TOOLTIP;
import static jcd.PropertyType.ZOOM_OUT_ICON;
import static jcd.PropertyType.ZOOM_OUT_TOOLTIP;
import jcd.controller.PageEditController;
import jcd.data.ClassObject;
import jcd.data.DataManager;
import jcd.data.MethodObject;
import jcd.data.VariableObject;
import properties_manager.PropertiesManager;
import saf.AppTemplate;
import saf.components.AppWorkspaceComponent;
import static saf.settings.AppStartupConstants.PATH_CODE;
import static saf.settings.AppStartupConstants.PATH_WORK;
import saf.ui.AppGUI;

/**
 *
 * @author rishab
 */
public class Workspace extends AppWorkspaceComponent
{
    AppTemplate app;
    
    HBox editToolbarPane;
    Button selectButton;
    Button addClassButton;
    Button addInterfaceButton;
    Button removeButton;
    Button undoButton;
    Button redoButton;
    
    CheckBox gridToggle;
    CheckBox snapToggle;
    
    HBox viewToolbarPane;
    Button zoomInButton;
    Button zoomOutButton;
    
    ScrollPane canvasContainer;
    Pane canvas;
    BorderPane workspaceBorderPane;
    VBox sidebarPane;
    HBox classBox;
    TextField classField;
    Label classLabel;
    HBox packageBox;
    TextField packageField;
    Label packageLabel;
    HBox  parentBox;
    ComboBox parentComboBox;
    Label parentLabel;
    HBox varBox;
    Label varLabel;
    Button addVarButton;
    Button removeVarButton; 
    ScrollPane varScroll;
    TableView varTable;
    HBox methodBox;
    Label methodLabel;
    Button addMethodButton;
    Button removeMethodButton;
    Button addArgButton;
    TableView methodTable;
    ScrollPane methodScroll;
    Button removeArgButton;
    HBox argBox;
        
    private boolean resetMode;
    
    PageEditController pageEditController;
    Button parentInterfacesButton;
    
    public Workspace(AppTemplate initApp)
    {
        File dir = new File(PATH_WORK);
        dir.mkdir();
        dir = new File(PATH_CODE);
        dir.mkdir();
                
        resetMode = false;
        app = initApp;
        AppGUI gui = app.getGUI();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        FlowPane toolbarPane = app.getGUI().getToolbar();
        
        pageEditController = new PageEditController(app);
        
        editToolbarPane = new HBox();
        selectButton = gui.initChildButton(editToolbarPane, SELECT_ICON.toString(), SELECT_TOOLTIP.toString(), true);
        selectButton.setOnAction(e -> {
            pageEditController.selectMode();
        });
        
               
        
        addClassButton = gui.initChildButton(editToolbarPane, ADD_CLASS_ICON.toString(), ADD_CLASS_TOOLTIP.toString(), true);
        addClassButton.setOnAction(e ->{
            pageEditController.addClassMode();
        });
        
        
        addInterfaceButton = gui.initChildButton(editToolbarPane, ADD_INTERFACE_ICON.toString(), ADD_INTERFACE_TOOLTIP.toString(), true);
        addInterfaceButton.setOnAction(e ->{
            pageEditController.addInterfaceMode();
        });
        
        removeButton = gui.initChildButton(editToolbarPane, REMOVE_ICON.toString(), REMOVE_TOOLTIP.toString(), true);
        removeButton.setOnAction(e ->{
            pageEditController.remove();
        });
        undoButton = gui.initChildButton(editToolbarPane, UNDO_ICON.toString(), UNDO_TOOLTIP.toString(), true);
        redoButton = gui.initChildButton(editToolbarPane, REDO_ICON.toString(),REDO_TOOLTIP.toString(),true);
        toolbarPane.getChildren().add(editToolbarPane);
        
        viewToolbarPane = new HBox();
        zoomInButton = gui.initChildButton(viewToolbarPane, ZOOM_IN_ICON.toString(), ZOOM_IN_TOOLTIP.toString(), true);
        zoomInButton.setOnAction(e->{
            
            if(canvas.getScaleX()<2)
            {
                canvas.setScaleX(canvas.getScaleX()+.05);
                canvas.setScaleY(canvas.getScaleY()+.05);
            }
        });
        zoomOutButton = gui.initChildButton(viewToolbarPane, ZOOM_OUT_ICON.toString(), ZOOM_OUT_TOOLTIP.toString(), true);
        zoomOutButton.setOnAction(e->{
            
            if(canvas.getScaleX()>1)
            {
                canvas.setScaleX(canvas.getScaleX()-.05);
                canvas.setScaleY(canvas.getScaleY()-.05);
            }
        });
        
        VBox viewToggles = new VBox();
        viewToggles.getStyleClass().add(CLASS_VERT_OPTIONS);
        
        gridToggle = new CheckBox(props.getProperty(GRID_LABEL.toString()));
        gridToggle.setDisable(true);
        gridToggle.setOnAction(e ->{            
            ((DataManager)app.getDataComponent()).setGrid(gridToggle.isSelected());
        });
        viewToggles.getChildren().add(gridToggle);
        
        snapToggle = new CheckBox(props.getProperty(SNAP_LABEL.toString()));
        snapToggle.setDisable(true);
        snapToggle.setOnAction(e->{
            
            pageEditController.setSnap(snapToggle.isSelected());
            if(snapToggle.isSelected())
            {
                for(ClassObject obj: ((DataManager)app.getDataComponent()).getClassObjects())
                {
                    double offset = obj.getX()%15;
                    if(offset <= 7.5)
                        obj.setX(obj.getX() - offset);
                    else
                        obj.setX(obj.getX() + (15-offset));
                    
                    offset = obj.getY()%15;
                    if(offset <= 7.5)
                        obj.setY(obj.getY() - offset);
                    else
                        obj.setY(obj.getY() + (15-offset));
                }
            }
        });
        viewToggles.getChildren().add(snapToggle);
        viewToolbarPane.getChildren().add(viewToggles);
        toolbarPane.getChildren().add(viewToolbarPane);
        
        canvas = new Pane();
        canvas.heightProperty().addListener(new ChangeListener<Number>() {
            
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue)
            {
                if(gridToggle.isSelected())
                    ((DataManager)app.getDataComponent()).expandGridV();
            }
        });
        canvas.widthProperty().addListener(new ChangeListener<Number>() {
            
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue)
            {
                if(gridToggle.isSelected())
                    ((DataManager)app.getDataComponent()).expandGridH();
            }
        });
//        canvas.setMinHeight(gui.getWindow().getHeight());
        pageEditController.setCanvas(canvas);
        canvasContainer = new ScrollPane();
        canvasContainer.setContent(canvas);
        canvasContainer.setOnMousePressed(e ->{
            pageEditController.handlePress(e);
        });
        

        sidebarPane = new VBox();
        classLabel = new Label(props.getProperty(CLASS_LABEL));
        classField = new TextField();
        classField.setDisable(true);
        classField.textProperty().addListener(new ChangeListener<String>() {

            @Override
            public void changed(ObservableValue<? extends String> observable,
                 String oldValue, String newValue) {
                    if(!resetMode)
                        pageEditController.checkIfNotValid(packageField.getText(),newValue);
                }
        });
        
        classBox = new HBox(5);
        classBox.getChildren().addAll(classLabel,classField);
        packageLabel = new Label(props.getProperty(PACKAGE_LABEL));
        packageField = new TextField();
        packageField.setDisable(true);
        packageField.textProperty().addListener(new ChangeListener<String>() {

            @Override
            public void changed(ObservableValue<? extends String> observable,
                 String oldValue, String newValue) {
                    if(!resetMode)
                        pageEditController.checkIfNotValid(newValue,classField.getText());
                  
                }
        });
        
        packageBox = new HBox(40);
        packageBox.getChildren().addAll(packageLabel,packageField);
        parentLabel = new Label(props.getProperty(PARENT_LABEL));
        parentComboBox = new ComboBox();
        parentComboBox.setDisable(true);
        parentComboBox.setPromptText("package.Class");
        parentComboBox.setMaxWidth(170);
        parentComboBox.setOnAction(e ->{
            if(!resetMode)
                pageEditController.setClassParent(((String)parentComboBox.getSelectionModel().getSelectedItem()).trim());
        });
        
        parentBox = new HBox(60);
        parentBox.getChildren().addAll(parentLabel,parentComboBox);
        
        parentInterfacesButton = new Button("Pick Implemented Interfaces");
        parentInterfacesButton.setOnAction(e -> {
            pageEditController.modifyInterfaces();
        });        
        parentInterfacesButton.setMinWidth(330);
        parentInterfacesButton.setDisable(true);
        
        varLabel = new Label(props.getProperty(VARIABLE_LABEL));
        varBox = new HBox(5);
        varBox.getChildren().add(varLabel);
        addVarButton = gui.initChildButton(varBox, ADD_ICON.toString(), ADD_VARIABLE_TOOLTIP.toString(), true);
        addVarButton.setOnAction(e -> {
            pageEditController.addVariable();
        });
        
        
        removeVarButton = gui.initChildButton(varBox, REMOVE_SIDE_ICON.toString(), REMOVE_VARIABLE_TOOLTIP.toString(), true);
        removeVarButton.setOnAction(e ->{
            pageEditController.removeVar((VariableObject) varTable.getSelectionModel().getSelectedItem());
        });
        
        varTable = new TableView();
        TableColumn nameColumn = new TableColumn("Name");
        nameColumn.setCellValueFactory(
                new PropertyValueFactory<VariableObject,String>("name")
        );
        TableColumn typeColumn = new TableColumn("Type");
        typeColumn.setCellValueFactory(
                new PropertyValueFactory<VariableObject,String>("type")
        );
        TableColumn staticColumn = new TableColumn("Static");
        staticColumn.setCellValueFactory(
                new PropertyValueFactory<VariableObject,Boolean>("static")
        );
        TableColumn accessColumn = new TableColumn("Access");
        accessColumn.setCellValueFactory(
                new PropertyValueFactory<VariableObject,String>("access")
        );
        varTable.getColumns().addAll(nameColumn,typeColumn,staticColumn,accessColumn);
        varTable.setRowFactory(e -> {
            TableRow<VariableObject> row = new TableRow<>();
            row.setOnMouseClicked(ev ->{
                if(ev.getClickCount() == 2 && !row.isEmpty())
                {
                    pageEditController.editVariable(row.getItem());
                }
            });
            
            return row;
        });
                
//        varScroll = new ScrollPane();
//        varScroll.setContent(varTable);
        methodLabel = new Label(props.getProperty(METHOD_LABEL));
        methodBox = new HBox(5);
        methodBox.getChildren().add(methodLabel);
        addMethodButton = gui.initChildButton(methodBox, ADD_ICON.toString(), ADD_METHOD_TOOLTIP.toString(), true);
        addMethodButton.setOnAction(e -> {
            pageEditController.addMethod();
        });
        
        removeMethodButton = gui.initChildButton(methodBox, REMOVE_SIDE_ICON.toString(), REMOVE_METHOD_TOOLTIP.toString(), true);
        removeMethodButton.setOnAction(e -> {
            pageEditController.removeMethod((MethodObject)methodTable.getSelectionModel().getSelectedItem());
        });
        
        methodTable = new TableView();
        methodTable.setRowFactory(e -> {
            TableRow<MethodObject> row = new TableRow<>();
            row.setOnMouseClicked(ev -> {
                if(ev.getClickCount() == 2 && !row.isEmpty())
                {
                    pageEditController.editMethod(row.getItem());
                }
            });
            
            return row;
        });
        TableColumn methodNameColumn = new TableColumn("Name");
        methodNameColumn.setCellValueFactory(
                new PropertyValueFactory<MethodObject,String>("name")
        );
        TableColumn returnColumn = new TableColumn("Return");
        returnColumn.setCellValueFactory(
                new PropertyValueFactory<MethodObject,String>("returnType")
        );
        TableColumn methodStaticColumn = new TableColumn("Static");
        methodStaticColumn.setCellValueFactory(
                new PropertyValueFactory<MethodObject,Boolean>("static")
        );
        TableColumn abstractColumn = new TableColumn("Abstract");
        abstractColumn.setCellValueFactory(
                new PropertyValueFactory<MethodObject,Boolean>("abstract")
        );
        TableColumn methodAccessColumn = new TableColumn("Access");
        methodAccessColumn.setCellValueFactory(
                new PropertyValueFactory<MethodObject,String>("access")
        );
        TableColumn argsColumn = new TableColumn("Arguments");
        argsColumn.setCellValueFactory(new Callback<CellDataFeatures<MethodObject,String>,ObservableValue<String>>(){
            
            @Override
            public ObservableValue<String> call(CellDataFeatures<MethodObject, String> param)
            {
                String str = "";
                for(VariableObject arg: param.getValue().getArgs())
                {
                    str += arg.getType() + " " + arg.getName() + ", ";
                }
                if(str.length() > 1)
                    str = str.substring(0, str.length()-2);
                
                return new SimpleStringProperty(str);
            }
            
        });
        methodTable.getColumns().addAll(methodNameColumn,returnColumn, methodStaticColumn,abstractColumn,methodAccessColumn,argsColumn);   
//        methodScroll = new ScrollPane();
//        methodScroll.setContent(methodTable);
//        methodScroll.setHbarPolicy(ScrollBarPolicy.ALWAYS);
//        argBox = new HBox(5);
//        addArgButton = gui.initChildButton(argBox, ADD_ARG_ICON.toString(), ADD_ARG_TOOLTIP.toString(), true);
//        removeArgButton = gui.initChildButton(argBox, REMOVE_ARG_ICON.toString(), REMOVE_ARG_TOOLTIP.toString(), true);
        sidebarPane.getChildren().addAll(classBox,packageBox,parentBox,parentInterfacesButton,varBox,varTable,methodBox,methodTable);
        sidebarPane.minWidthProperty().set(350);
        sidebarPane.maxWidthProperty().set(350);
        
        workspaceBorderPane = new BorderPane();
        workspaceBorderPane.minWidthProperty().bind(gui.getWindow().widthProperty());
        workspaceBorderPane.maxWidthProperty().bind(gui.getWindow().widthProperty());
        workspaceBorderPane.minHeightProperty().bind(gui.getWindow().heightProperty().subtract(toolbarPane.heightProperty()));
        workspaceBorderPane.maxHeightProperty().bind(gui.getWindow().heightProperty().subtract(toolbarPane.heightProperty()));
        canvas.minHeightProperty().bind(workspaceBorderPane.heightProperty());
        canvas.minWidthProperty().bind(workspaceBorderPane.widthProperty().subtract(sidebarPane.widthProperty()));
//        sidebarPane.maxHeightProperty().bind(gui.getWindow().heightProperty());
        workspaceBorderPane.setCenter(canvasContainer);
        workspaceBorderPane.setRight(sidebarPane);
        
        workspace = new Pane();
        workspace.getChildren().add(workspaceBorderPane);
        
        workspaceActivated = false;
    }

    @Override
    public void reloadWorkspace()
    {
        ClassObject obj = pageEditController.reset();
        if(obj != null)
            deActivateSidePane(obj);
        classSelected(false);
    }

    @Override
    public void initStyle()
    {
        editToolbarPane.getStyleClass().add(CLASS_EDIT_PANE);
        viewToolbarPane.getStyleClass().add(CLASS_VIEW_PANE);
        selectButton.getStyleClass().add(CLASS_TOOLBAR_BUTTON);
        addClassButton.getStyleClass().add(CLASS_TOOLBAR_BUTTON);
        addInterfaceButton.getStyleClass().add(CLASS_TOOLBAR_BUTTON);
        removeButton.getStyleClass().add(CLASS_TOOLBAR_BUTTON);
        undoButton.getStyleClass().add(CLASS_TOOLBAR_BUTTON);
        redoButton.getStyleClass().add(CLASS_TOOLBAR_BUTTON);
        zoomInButton.getStyleClass().add(CLASS_TOOLBAR_BUTTON);
        zoomOutButton.getStyleClass().add(CLASS_TOOLBAR_BUTTON);
        gridToggle.getStyleClass().add(CLASS_VIEW_TOGGLES);
        snapToggle.getStyleClass().add(CLASS_VIEW_TOGGLES);
        sidebarPane.getStyleClass().add(CLASS_SIDE_PANE);
        classLabel.getStyleClass().add(CLASS_TEXT);
        packageLabel.getStyleClass().add(CLASS_TEXT);
        parentLabel.getStyleClass().add(CLASS_TEXT);
        addVarButton.getStyleClass().add(CLASS_SIDEBAR_BUTTON);
        removeVarButton.getStyleClass().add(CLASS_SIDEBAR_BUTTON);
        varLabel.getStyleClass().add(CLASS_TEXT);
        addMethodButton.getStyleClass().add(CLASS_SIDEBAR_BUTTON);
        removeMethodButton.getStyleClass().add(CLASS_SIDEBAR_BUTTON);
        methodLabel.getStyleClass().add(CLASS_TEXT);
        varTable.getStyleClass().add(CLASS_TABLE);
        methodTable.getStyleClass().add(CLASS_TABLE);
    }
    
    @Override
     public void activateWorkspace(BorderPane appPane) 
     {
        if (!workspaceActivated) 
        {
            selectButton.setDisable(false);
            addClassButton.setDisable(false);
            addInterfaceButton.setDisable(false);
            zoomInButton.setDisable(false);
            zoomOutButton.setDisable(false);
            DataManager dataManager = (DataManager) app.getDataComponent();
            dataManager.setCanvas(canvas);
            dataManager.setPageEditController(pageEditController);
            app.getGUI().activateControls();
            gridToggle.setDisable(false);
            snapToggle.setDisable(false);
        }
        super.activateWorkspace(appPane);
     }
          
     public void activateSidePane(ClassObject obj)
     {
         resetMode = true;
         classField.setDisable(false);
         classField.textProperty().bindBidirectional(obj.getClassNameProperty());
         packageField.setDisable(false);
         packageField.textProperty().bindBidirectional(obj.getPackageProperty());
         parentComboBox.setDisable(obj.getType().equals("interface"));
         parentComboBox.setEditable(true);
         parentComboBox.getItems().clear();
         parentComboBox.setValue("");
         ObservableList vars = FXCollections.observableArrayList();
         vars.addAll(obj.getVariables());
         varTable.getItems().clear();
         varTable.setItems(vars);
         ObservableList methods = FXCollections.observableArrayList();
         methods.addAll(obj.getMethods());
         methodTable.getItems().clear();
         methodTable.setItems(methods);
         ArrayList<ClassObject> classes = ((DataManager) app.getDataComponent()).getClassObjects();
         
         for (ClassObject classObject : classes)
         {
             if(classObject != obj && !classObject.getType().equals("interface"))
             {
               String str = classObject.toString();
               parentComboBox.getItems().add(str);
               
               if(obj.getParentClass() != null && str.equals(obj.getParentClass().toString()))
                     parentComboBox.getSelectionModel().select(parentComboBox.getItems().size()-1);
             }
         }
         if(obj.isExternal())
         {
             classField.setMouseTransparent(true);
             packageField.setMouseTransparent(true);
             parentComboBox.setMouseTransparent(true);
             addVarButton.setDisable(true);
             removeVarButton.setDisable(true);
             addMethodButton.setDisable(true);
             removeMethodButton.setDisable(true);
             parentInterfacesButton.setDisable(true);
             
         }
         else
         {
             classField.setMouseTransparent(false);
             packageField.setMouseTransparent(false);
             parentComboBox.setMouseTransparent(false);
             addVarButton.setDisable(false);
             removeVarButton.setDisable(false);
             addMethodButton.setDisable(false);
             removeMethodButton.setDisable(false);
             parentInterfacesButton.setDisable(false);
         }
         
         resetMode = false;
     }

    public void deActivateSidePane(ClassObject obj)
    {
        resetMode = true;
        unbindEditors(obj);
        classField.setDisable(true);
        classField.setText("");
        packageField.setDisable(true);
        packageField.setText("");
        parentComboBox.setDisable(true);
        parentComboBox.getItems().clear();
        addVarButton.setDisable(true);
        removeVarButton.setDisable(true);
        addMethodButton.setDisable(true);
        removeMethodButton.setDisable(true);
        parentInterfacesButton.setDisable(true);
        varTable.getItems().clear();
        methodTable.getItems().clear();
        resetMode = false;
    }
    
    public void unbindEditors(ClassObject obj)
    {
        packageField.textProperty().unbindBidirectional(obj.getPackageProperty());
        classField.textProperty().unbindBidirectional(obj.getClassNameProperty());   
    }
    
    public void classSelected(boolean sel)
    {
        removeButton.setDisable(!sel);
    }
}