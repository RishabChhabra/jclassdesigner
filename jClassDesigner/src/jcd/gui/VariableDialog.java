/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.gui;

import java.util.ArrayList;
import java.util.List;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import jcd.data.ClassObject;
import jcd.data.VariableObject;
import org.controlsfx.control.CheckComboBox;


/**
 *
 * @author rishab
 */
public class VariableDialog extends Stage
{
    VBox messagePane;
    Scene messageScene;
    Button okButton;
    Button cancelButton;
    String selection;
    ComboBox<String> typeComboBox;
    TextField nameTextField;
    CheckBox staticCheckBox;
    ComboBox<String> accessComboBox;
    
    // CONSTANT CHOICES
    public static final String OK = "Okay";
    public static final String CANCEL = "Cancel";
    
    public VariableDialog() {}

    public VariableDialog(Stage primaryStage) 
    {
        init(primaryStage);
    }

	

    public void init(Stage primaryStage) {
        // MAKE THIS DIALOG MODAL, MEANING OTHERS WILL WAIT
        // FOR IT WHEN IT IS DISPLAYED
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        
        selection = "";
                
        // YES, NO, AND CANCEL BUTTONS
        okButton = new Button(OK);
        cancelButton = new Button(CANCEL);
	// MAKE THE EVENT HANDLER FOR THESE BUTTONS
        EventHandler okHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            Button sourceButton = (Button)ae.getSource();
            VariableDialog.this.selection = sourceButton.getText();
            VariableDialog.this.hide();
        };
        
	// AND THEN REGISTER THEM TO RESPOND TO INTERACTIONS
        okButton.setOnAction(okHandler);
        cancelButton.setOnAction(okHandler);

        // NOW ORGANIZE OUR BUTTONS
        HBox buttonBox = new HBox();
        buttonBox.getChildren().addAll(okButton,cancelButton);
        
        
        
        
        Label typeLabel = new Label("Type:");
        typeComboBox = new ComboBox<>();
        typeComboBox.setPromptText("package.class");
        typeComboBox.setEditable(true);
        HBox typeBox = new HBox();
        typeBox.getChildren().addAll(typeLabel,typeComboBox);
        
        Label nameLabel = new Label("Name:");
        nameTextField = new TextField();
        HBox nameBox = new HBox();
        nameBox.getChildren().addAll(nameLabel,nameTextField);
        
        Label staticLabel = new Label("Static:");
        staticCheckBox = new CheckBox();
        HBox staticBox = new HBox();
        staticBox.getChildren().addAll(staticLabel,staticCheckBox);
        
        Label accessLabel = new Label("Access:");
        accessComboBox = new ComboBox<>();
        accessComboBox.setEditable(false);
        accessComboBox.getItems().addAll("public","private","protected");
        accessComboBox.getSelectionModel().select(2);
        HBox accessBox = new HBox();
        accessBox.getChildren().addAll(accessLabel,accessComboBox);
       
        // WE'LL PUT EVERYTHING HERE
        messagePane = new VBox();
        messagePane.setAlignment(Pos.CENTER_LEFT);                
        messagePane.getChildren().addAll(typeBox,nameBox,staticBox,accessBox,buttonBox);        
        // MAKE IT LOOK NICE
        messagePane.setPadding(new Insets(10, 20, 20, 20));
        messagePane.setSpacing(10);

        // AND PUT IT IN THE WINDOW
        messageScene = new Scene(messagePane);
        this.setScene(messageScene);
    }

    /**
     * Accessor method for getting the selection the user made.
     * 
     * @return Either YES or NO, depending on which
     * button the user selected when this dialog was presented.
     */
    public String getSelection() 
    {
        return selection;
    }
    
    public String getName()
    {
        return  nameTextField.getText().trim();
    }

    public String getType()
    {
        return typeComboBox.getSelectionModel().getSelectedItem().trim();
    }
    
    public boolean getStatic()
    {
        return staticCheckBox.isSelected();
    }
    
    public String getAccess()
    {
        return accessComboBox.getSelectionModel().getSelectedItem().trim();
    }
 
    /**
     * This method loads a custom message into the label
     * then pops open the dialog.
     * 
     * @param title The title to appear in the dialog window bar.
     * 
     * @param message Message to appear inside the dialog.
     */
    public void show(ArrayList<ClassObject> all)
    {
        setTitle("New Variable");
        
        for(ClassObject obj: all)
        {
            typeComboBox.getItems().add(obj.toString());
        }
        
        showAndWait();
    }    

    public void show(ArrayList<ClassObject> all, VariableObject item) 
    {
        setTitle("New Variable");
        String fullType = item.getType();
        if(!item.getImportPackage().equals(""))
            fullType = item.getImportPackage() + "." + item.getType();
        
        for(ClassObject obj: all)
        {
            typeComboBox.getItems().add(obj.toString());
        }
        typeComboBox.getSelectionModel().select(fullType);
        nameTextField.setText(item.getName());
        staticCheckBox.setSelected(item.isStatic());
        accessComboBox.getSelectionModel().select(item.getAccess());
        
        showAndWait();        
    }
    
}
