/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.controller;

import jcd.gui.FullExternalClassDialogSingleton;
import java.util.ArrayList;
import javafx.collections.ObservableList;
import javafx.scene.control.Dialog;
import javafx.scene.control.ScrollPane;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import jcd.PropertyType;
import static jcd.PropertyType.EMPTY_NAME_ERROR;
import static jcd.PropertyType.GET_EXTERNAL_INFO_MESSAGE;
import static jcd.PropertyType.GET_EXTERNAL_INFO_TITLE;
import static jcd.PropertyType.INTERFACES_SKIPPED_MESSAGE;
import static jcd.PropertyType.INVALID_COMBO_MESSAGE;
import static jcd.PropertyType.INVALID_COMBO_TITLE;
import static jcd.PropertyType.INVALID_NEW_COMBO_MESSAGE;
import static jcd.PropertyType.INVALID_PARENT_TITLE;
import static jcd.PropertyType.IVALID_PARENT_MESSAGE;
import static jcd.PropertyType.NO_TYPE_ERROR;
import static jcd.PropertyType.PARENT_NOT_SAVED;
import static jcd.PropertyType.VARIABLE_ADD_ERROR;
import static jcd.PropertyType.VARIABLE_NOT_SAVED;
import jcd.data.ClassObject;
import jcd.data.DataManager;
import jcd.data.MethodObject;
import jcd.data.VariableObject;
import jcd.gui.ExternalClassDialogSingleton;
import jcd.gui.InterfaceDialog;
import jcd.gui.MethodDialog;
import jcd.gui.VariableDialog;
import jcd.gui.Workspace;
import properties_manager.PropertiesManager;
import saf.AppTemplate;
import saf.ui.AppMessageDialogSingleton;
import sun.reflect.generics.tree.ReturnType;

/**
 *
 * @author rishab
 */
public class PageEditController
{
    private AppTemplate app;
    private DataManager data;
    private Pane canvas;
    private boolean select;
    private boolean addInterface;
    private boolean addClass;
    private ClassObject currentClass;
    private long timeSelected;
    private DropShadow selection;
    private double offsetX;
    private double offsetY;
    private String[] oldInfo; 
    private boolean invalid;
    private boolean snap;

    public PageEditController(AppTemplate initApp)
    {
        app = initApp;
        data = (DataManager) app.getDataComponent();
        canvas = null;
        select = false;
        addInterface = false;
        addClass = true;   
        oldInfo = new String[2];
        selection = new DropShadow();
        selection.setColor(Color.YELLOW);
        selection.setSpread(.75);
        invalid = false;
        snap = false;
    }
    
    public void selectMode()
    {
        if(currentClass != null)
        {
            ((Workspace) app.getWorkspaceComponent()).classSelected(true);
        }
        addClass = false;
        addInterface = false;
        select = true;
        ClassObject.setResizeable(true);
    }
    
    public void addClassMode()
    {
        if (!addClass && currentClass != null) 
        {
            currentClass.setEffect(null);
            ((Workspace) app.getWorkspaceComponent()).deActivateSidePane(currentClass);
        }
       addClass = true;
       addInterface = false;
       select = false;
       ((Workspace) app.getWorkspaceComponent()).classSelected(false);
       ClassObject.setResizeable(false);

    }
    
    public void addInterfaceMode() 
    {
        if (!addInterface && currentClass != null) 
        {
            currentClass.setEffect(null);
            ((Workspace) app.getWorkspaceComponent()).deActivateSidePane(currentClass);
        }
       addClass = false;
       addInterface = true;
       select = false;
       ((Workspace) app.getWorkspaceComponent()).classSelected(false);
       ClassObject.setResizeable(false);
    }
    
    public void handlePress(MouseEvent e)
    {
        if(!select)
        {
            if(currentClass != null)
            {
                manageInvalid();
                ((Workspace) app.getWorkspaceComponent()).deActivateSidePane(currentClass);
                currentClass.setEffect(null);
            }
            String classType = "";
            if(addClass)
                classType = "class";
            else if(addInterface)
                classType = "interface";
            ClassObject cl = new ClassObject(canvas,classType,false);
            cl.setX(e.getX());
            cl.setY(e.getY());
            cl.setWidth(150);
            cl.setHeight(150);
            cl.setEffect(selection);
            
            app.getGUI().updateToolbarControls(false);
            currentClass = cl;
            oldInfo[0] = cl.getPkg();
            oldInfo[1] = cl.getName();
            data.addClass(cl);
            addListeners(cl);
            ((Workspace)app.getWorkspaceComponent()).activateSidePane(cl);
            checkIfNotValid(cl.getPkg(), cl.getName());
        }
        else if(select && e.getSource() instanceof ScrollPane && currentClass != null && (System.currentTimeMillis() - timeSelected) > 10)
        {
            manageInvalid();
            ((Workspace) app.getWorkspaceComponent()).deActivateSidePane(currentClass);
            ((Workspace) app.getWorkspaceComponent()).classSelected(false);
            currentClass.setEffect(null);
            currentClass = null;
        }
    }

    public void setCanvas(Pane pane)
    {
        canvas = pane;
    }
    
    public void addListeners(ClassObject cl)
    {
        cl.setOnMousePressed(e -> {
            
            if (select) 
            {
                if (currentClass != null) 
                {
                    if( e.getSource() != currentClass)
                    {
                        currentClass.setEffect(null);
                        manageInvalid();
                    }
                    ((Workspace)app.getWorkspaceComponent()).unbindEditors(currentClass);
                }
                currentClass = (ClassObject) e.getSource();
                offsetX = e.getX() - (currentClass).getX();
                offsetY = e.getY() - (currentClass).getY();
                currentClass.setEffect(selection);
                oldInfo[0] = currentClass.getPkg();
                oldInfo[1] = currentClass.getName();
                ((Workspace)app.getWorkspaceComponent()).activateSidePane(currentClass);
                ((Workspace) app.getWorkspaceComponent()).classSelected(true);
                timeSelected = System.currentTimeMillis();
            }
        });
        cl.setOnMouseDragged(e -> {
            if (select) 
            {
                double newX = e.getX()-offsetX;
                double newY = e.getY()-offsetY;
                if(newX < 0)
                    newX = 0;
                if(newY  < 0)
                    newY = 0;
                
                currentClass.setX(newX);
                currentClass.setY(newY);
                app.getGUI().updateToolbarControls(false);
            }
        }); 
        cl.setOnMouseReleased(e -> {
            if(snap)
            {
                double offset = currentClass.getX() % 15;
                if (offset <= 7.5) {
                    currentClass.setX(currentClass.getX() - offset);
                } else {
                    currentClass.setX(currentClass.getX() + (15 - offset));
                }

                offset = currentClass.getY() % 15;
                if (offset <= 7.5) {
                    currentClass.setY(currentClass.getY() - offset);
                } else {
                    currentClass.setY(currentClass.getY() + (15 - offset));
                }
            }
        });
    }

    public  void checkIfNotValid(String pkg, String name)
    {
        invalid = data.isRepeat(currentClass, pkg, name);
        if(invalid)
        {
            if(data.hasOld(currentClass,oldInfo))
            {
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                PropertiesManager props = PropertiesManager.getPropertiesManager();
                dialog.show(props.getProperty(INVALID_COMBO_TITLE), props.getProperty(INVALID_NEW_COMBO_MESSAGE));
            }
            else
            {
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                PropertiesManager props = PropertiesManager.getPropertiesManager();
                dialog.show(props.getProperty(INVALID_COMBO_TITLE), props.getProperty(INVALID_COMBO_MESSAGE));
            }
        }
    }
    
    private void manageInvalid()
    {
         if (invalid) 
         {
            if (data.hasOld(currentClass,oldInfo)) 
            {
                currentClass.removeSelf();
                data.removeClass(currentClass);
            }
            else 
            {
                currentClass.setName(oldInfo[1]);
                currentClass.setPkg(oldInfo[0]);
            }
            invalid = false;
        }
    }

    public ClassObject reset()
    {
        ClassObject temp = currentClass;
        currentClass = null;
        select = false;
        addInterface = false;
        addClass = true;   
        return temp;
    }
    
    public void remove()
    {
        if(currentClass != null)
        {
            data.removeClass(currentClass);
            currentClass.removeSelf();
            
            for(ClassObject obj: data.getClassObjects())
            {
                obj.renderRelationships(data);
            }
            ((Workspace) app.getWorkspaceComponent()).classSelected(false);
            ((Workspace) app.getWorkspaceComponent()).deActivateSidePane(currentClass);
            app.getGUI().updateToolbarControls(false);         
            currentClass = null;
        }
    }

    public void setClassParent(String string) 
    {
        if(currentClass != null)
        {
            if(string.equals(""))
            {
                currentClass.setParent(null);
                currentClass.renderRelationships(data);
            }
            else
            {
                PropertiesManager props = PropertiesManager.getPropertiesManager();
                String pack = "";
                String name = string;
                if (string.contains("."))
                {
                    pack = string.substring(0, string.lastIndexOf("."));
                    name = string.substring(string.lastIndexOf(".") + 1);
                }
                if(name.equals(""))
                {
                    AppMessageDialogSingleton.getSingleton().show(PropertiesManager.getPropertiesManager().getProperty(INVALID_PARENT_TITLE),
                            PropertiesManager.getPropertiesManager().getProperty(IVALID_PARENT_MESSAGE));
                }
                else
                {
                    ClassObject parent = ((DataManager) app.getDataComponent()).getClass(data.extractClass(name), pack);
                
                    if (data.isPrimitive(data.extractClass(name), pack) || parent != null)
                    {
                        currentClass.setParent(parent);
                        currentClass.renderRelationships(data);
                        app.getGUI().updateToolbarControls(false);
                    } 
                    else 
                    {
                        ExternalClassDialogSingleton dialog = ExternalClassDialogSingleton.getSingleton();
                        dialog.show(PropertiesManager.getPropertiesManager().getProperty(GET_EXTERNAL_INFO_TITLE),PropertiesManager.getPropertiesManager().getProperty(GET_EXTERNAL_INFO_MESSAGE));
                        if(dialog.getSelection().equals("Okay"))
                        {
                            parent = new ClassObject(canvas, dialog.getClassType(), true);
                            parent.setX(0);
                            parent.setY(0);
                            parent.setWidth(150);
                            parent.setHeight(150);
                            parent.setName(name);
                            parent.setPkg(pack);
                            currentClass.setParent(parent);
                            addListeners(parent);
                            data.addClass(parent);
                            currentClass.renderRelationships(data);
                            app.getGUI().updateToolbarControls(false);
                             
                        }
                        else
                        {
                            AppMessageDialogSingleton.getSingleton().show(PropertiesManager.getPropertiesManager().getProperty(INVALID_COMBO_TITLE),
                                PropertiesManager.getPropertiesManager().getProperty(PARENT_NOT_SAVED));
                        }
                    }
                }
            }
        }
    }

    public void setSnap(boolean sn) 
    {
        snap = sn;
    }

    public void modifyInterfaces() 
    {
        InterfaceDialog dialog = new InterfaceDialog(app.getGUI().getWindow());
        dialog.show(currentClass,data.getClassObjects());
        if(dialog.getSelection() != null)
        {
            ArrayList<String> toImplement = dialog.getInterfaces();
            //Create External Interfaces
            boolean skipped = false;
            for(String str: toImplement)
            {
                String pack = "";
                String name = str;
                if (str.contains("."))
                {
                    pack = str.substring(0, str.lastIndexOf("."));
                    name = str.substring(str.lastIndexOf(".") + 1);
                }
                
                if(name.equals(""))
                {
                    skipped = true;
                }
                else
                {
                    ClassObject temp = ((DataManager) app.getDataComponent()).getClass(data.extractClass(name), pack);
                    if(!data.isPrimitive(data.extractClass(name), pack)  && temp == null)
                    {
                        temp = new ClassObject(canvas, "interface", true);
                        temp.setX(0);
                        temp.setY(0);
                        temp.setWidth(150);
                        temp.setHeight(150);
                        temp.setName(name);
                        temp.setPkg(pack);
                        addListeners(temp);
                        data.addClass(temp);
                    }
                }
            }
            
            currentClass.implementInterfaces(toImplement,data);
            currentClass.renderRelationships(data);
            app.getGUI().updateToolbarControls(false);
            
            if(skipped)
                AppMessageDialogSingleton.getSingleton().show("", PropertiesManager.getPropertiesManager().getProperty(INTERFACES_SKIPPED_MESSAGE));
        }
            
    }
    
    public void addVariable()
    {
        VariableDialog dialog = new VariableDialog(app.getGUI().getWindow());
        dialog.show(data.getClassObjects());
        
        if(dialog.getSelection().equals("Okay"))
        {
            String name = dialog.getName();
            if(name.equals(""))
            {
                AppMessageDialogSingleton.getSingleton().show(PropertiesManager.getPropertiesManager().getProperty(VARIABLE_ADD_ERROR),
                                    PropertiesManager.getPropertiesManager().getProperty(EMPTY_NAME_ERROR));//empty name error
            }
            else
            {
                if (repeatVariable(name,null)) 
                {
                    AppMessageDialogSingleton.getSingleton().show();//repeat name error                    
                } 
                else
                {
                    String pack = "";
                    String type = dialog.getType();                    
                    if(type == null)
                    {
                        AppMessageDialogSingleton.getSingleton().show(PropertiesManager.getPropertiesManager().getProperty(VARIABLE_ADD_ERROR),
                                    PropertiesManager.getPropertiesManager().getProperty(NO_TYPE_ERROR));//No type error
                    }
                    else
                    {
                        if (type.contains("."))
                        {
                            pack = type.substring(0, type.lastIndexOf("."));
                            type = type.substring(type.lastIndexOf(".") + 1);
                        }
                        if(type.equals(""))
                        {
                            AppMessageDialogSingleton.getSingleton().show(PropertiesManager.getPropertiesManager().getProperty(VARIABLE_ADD_ERROR),
                                    PropertiesManager.getPropertiesManager().getProperty(NO_TYPE_ERROR));//No type error
                        }
                        else
                        {
                            if (!checkExternal(type, pack))
                            {
                                AppMessageDialogSingleton.getSingleton().show(PropertiesManager.getPropertiesManager().getProperty(INVALID_COMBO_TITLE),
                                        PropertiesManager.getPropertiesManager().getProperty(VARIABLE_NOT_SAVED));
                            }
                            else
                            {                           

                                VariableObject var = new VariableObject();
                                var.setName(name);
                                var.setType(type);
                                var.setStatic(dialog.getStatic());
                                if (dialog.getAccess() != null) 
                                {
                                    var.setAcccess(dialog.getAccess());
                                }
                                var.setImportPackage(pack);
                                currentClass.addVariable(var);
                                currentClass.renderRelationships(data);
                                ((Workspace) app.getWorkspaceComponent()).deActivateSidePane(currentClass);
                                ((Workspace) app.getWorkspaceComponent()).activateSidePane(currentClass);
                                app.getGUI().updateToolbarControls(false);
                            }
                        }

                    }
                }
            }
        }
    }

    private boolean repeatVariable(String name,VariableObject test) 
    {
        for(VariableObject var: currentClass.getVariables())
        {
            if(var != test && var.getName().equals(name))
                return true;
        }
        return false;
    }

    public void removeVar(VariableObject variable) 
    {
        if(variable != null)
        {
            currentClass.removeVariable(variable);
            currentClass.renderRelationships(data);
            app.getGUI().updateToolbarControls(false);
            ((Workspace) app.getWorkspaceComponent()).deActivateSidePane(currentClass);
            ((Workspace) app.getWorkspaceComponent()).activateSidePane(currentClass);
        }
    }

    public void removeMethod(MethodObject methodObject)
    {
        if(methodObject !=  null)
        {
            currentClass.removeMethod(methodObject);
            currentClass.renderRelationships(data);
            app.getGUI().updateToolbarControls(false);
            ((Workspace) app.getWorkspaceComponent()).deActivateSidePane(currentClass);
            ((Workspace) app.getWorkspaceComponent()).activateSidePane(currentClass);
        }
    }

    public void addMethod() 
    {
        MethodDialog dialog = new MethodDialog(app.getGUI().getWindow());
        dialog.show(data.getClassObjects(),currentClass.getType().equals("interface"));
        
        if(dialog.getSelection().equals("Okay"))
        {
            String name = dialog.getName();
            if(!name.isEmpty())
            {
                String returnType;
                if(name.equals(currentClass.getName()))
                {
                     returnType = "constructor";
                }
                else
                {
                    returnType = dialog.getReturnType();
                    if (returnType.equals("") || returnType.equals("void"))
                    {
                        returnType = "void";
                    }
                }
                
                String pack = "";
                if (returnType.contains("."))
                {
                    pack = returnType.substring(0, returnType.lastIndexOf("."));
                    returnType = returnType.substring(returnType.lastIndexOf(".") + 1);
                }
                if (returnType.equals("")) 
                {
                    AppMessageDialogSingleton.getSingleton().show(PropertiesManager.getPropertiesManager().getProperty(VARIABLE_ADD_ERROR),
                            PropertiesManager.getPropertiesManager().getProperty(NO_TYPE_ERROR));//No type error
                } 
                else if (!returnType.equals("void") && !checkExternal(returnType, pack)) 
                {
                    AppMessageDialogSingleton.getSingleton().show(PropertiesManager.getPropertiesManager().getProperty(INVALID_COMBO_TITLE),
                            PropertiesManager.getPropertiesManager().getProperty(VARIABLE_NOT_SAVED));
                }
                else 
                {
                    MethodObject method = new MethodObject();
                    method.setAbstract(dialog.getAbstract());
                    method.setAccess(dialog.getAccess());
                    method.setReturnType(returnType);
                    method.setReturnPackage(pack);
                    method.setName(name);
                    method.setStatic(dialog.getStatic());
                    
                    ArrayList<String[]> args = dialog.getArguments();
                    ArrayList<VariableObject> vars = new ArrayList();
                    for(String[] data: args)
                    {
                        String pkg = "";
                        String type = data[0];
                        if (type.contains("."))
                        {
                            pkg = type.substring(0, type.lastIndexOf("."));
                            type = type.substring(type.lastIndexOf(".") + 1);
                        }
                        checkExternal(type, pack);
                        VariableObject arg = new VariableObject();
                        arg.setAcccess("arg");
                        arg.setImportPackage(pkg);
                        arg.setName(data[1]);
                        arg.setType(type);
                        vars.add(arg);
                    }               
                    
                    method.setArgs(vars);
                    currentClass.addMethod(method);
                    currentClass.renderRelationships(data);
                    ((Workspace) app.getWorkspaceComponent()).deActivateSidePane(currentClass);
                    ((Workspace) app.getWorkspaceComponent()).activateSidePane(currentClass);
                    app.getGUI().updateToolbarControls(false);
                }
                
            }
        }
    }

    private boolean checkExternal(String name,String pack) 
    {
        name = data.extractClass(name);
        if (!data.isPrimitive(name,pack) && data.getClass(name, pack) == null) 
        {
            FullExternalClassDialogSingleton dialog = FullExternalClassDialogSingleton.getSingleton();
            dialog.show(PropertiesManager.getPropertiesManager().getProperty(GET_EXTERNAL_INFO_TITLE),
                    name + " is a(n):");
            if (dialog.getSelection().equals("Okay")) 
            {
                ClassObject obj = new ClassObject(canvas, dialog.getClassType(), true);
                obj.setX(0);
                obj.setY(0);
                obj.setWidth(150);
                obj.setHeight(150);
                obj.setName(name);
                obj.setPkg(pack);
                addListeners(obj);
                data.addClass(obj);
                
                return true;
            }
            return false;
        }
        return true;
    }

    public void editVariable(VariableObject item) 
    {
        VariableDialog dialog = new VariableDialog(app.getGUI().getWindow());
        dialog.show(data.getClassObjects(),item);
        
        if(dialog.getSelection().equals("Okay"))
        {
            String name = dialog.getName();
            if(name.equals(""))
            {
                AppMessageDialogSingleton.getSingleton().show(PropertiesManager.getPropertiesManager().getProperty(VARIABLE_ADD_ERROR),
                                    PropertiesManager.getPropertiesManager().getProperty(EMPTY_NAME_ERROR));//empty name error
            }
            else
            {
                if (repeatVariable(name,item)) 
                {
                    AppMessageDialogSingleton.getSingleton().show();//repeat name error                    
                } 
                else
                {
                    String pack = "";
                    String type = dialog.getType();                    
                    if(type == null)
                    {
                        AppMessageDialogSingleton.getSingleton().show(PropertiesManager.getPropertiesManager().getProperty(VARIABLE_ADD_ERROR),
                                    PropertiesManager.getPropertiesManager().getProperty(NO_TYPE_ERROR));//No type error
                    }
                    else
                    {
                        if (type.contains("."))
                        {
                            pack = type.substring(0, type.lastIndexOf("."));
                            type = type.substring(type.lastIndexOf(".") + 1);
                        }
                        if(type.equals(""))
                        {
                            AppMessageDialogSingleton.getSingleton().show(PropertiesManager.getPropertiesManager().getProperty(VARIABLE_ADD_ERROR),
                                    PropertiesManager.getPropertiesManager().getProperty(NO_TYPE_ERROR));//No type error
                        }
                        else
                        {
                            if (!checkExternal(type, pack))
                            {
                                AppMessageDialogSingleton.getSingleton().show(PropertiesManager.getPropertiesManager().getProperty(INVALID_COMBO_TITLE),
                                        PropertiesManager.getPropertiesManager().getProperty(VARIABLE_NOT_SAVED));
                            }
                            else
                            {                           

                                item.setName(name);
                                item.setType(type);
                                item.setStatic(dialog.getStatic());
                                item.setAcccess(dialog.getAccess());
                                item.setImportPackage(pack);
                                ((Workspace) app.getWorkspaceComponent()).deActivateSidePane(currentClass);
                                ((Workspace) app.getWorkspaceComponent()).activateSidePane(currentClass);
                                app.getGUI().updateToolbarControls(false);
                                currentClass.renderRelationships(data);
                                currentClass.render();
                            }
                        }

                    }
                }
            }
        }        
    }
    
    public void editMethod(MethodObject item)
    {
        MethodDialog dialog = new MethodDialog(app.getGUI().getWindow());
        dialog.show(data.getClassObjects(),item,currentClass.getType().equals("interface"));
        
        if(dialog.getSelection().equals("Okay"))
        {
            String name = dialog.getName();
            if(!name.isEmpty())
            {
                String returnType;
                if(name.equals(currentClass.getName()))
                {
                     returnType = "constructor";
                }
                else
                {
                    returnType = dialog.getReturnType();
                    if (returnType.equals("") || returnType.equals("void"))
                    {
                        returnType = "void";
                    }
                }
                
                String pack = "";
                if (returnType.contains("."))
                {
                    pack = returnType.substring(0, returnType.lastIndexOf("."));
                    returnType = returnType.substring(returnType.lastIndexOf(".") + 1);
                }
                if (returnType.equals("")) 
                {
                    AppMessageDialogSingleton.getSingleton().show(PropertiesManager.getPropertiesManager().getProperty(VARIABLE_ADD_ERROR),
                            PropertiesManager.getPropertiesManager().getProperty(NO_TYPE_ERROR));//No type error
                } 
                else if (!returnType.equals("void") && !checkExternal(returnType, pack)) 
                {
                    AppMessageDialogSingleton.getSingleton().show(PropertiesManager.getPropertiesManager().getProperty(INVALID_COMBO_TITLE),
                            PropertiesManager.getPropertiesManager().getProperty(VARIABLE_NOT_SAVED));
                }
                else 
                {
                    item.setAbstract(dialog.getAbstract());
                    item.setAccess(dialog.getAccess());
                    item.setReturnType(returnType);
                    item.setReturnPackage(pack);
                    item.setName(name);
                    item.setStatic(dialog.getStatic());
                    
                    ArrayList<String[]> args = dialog.getArguments();
                    ArrayList<VariableObject> vars = new ArrayList();
                    for(String[] data: args)
                    {
                        String pkg = "";
                        String type = data[0];
                        if (type.contains("."))
                        {
                            pkg = type.substring(0, type.lastIndexOf("."));
                            type = type.substring(type.lastIndexOf(".") + 1);
                        }
                        checkExternal(type, pkg);
                        VariableObject arg = new VariableObject();
                        arg.setAcccess("arg");
                        arg.setImportPackage(pkg);
                        arg.setName(data[1]);
                        arg.setType(type);
                        vars.add(arg);
                    }               
                    
                    item.setArgs(vars);
                    ((Workspace) app.getWorkspaceComponent()).deActivateSidePane(currentClass);
                    ((Workspace) app.getWorkspaceComponent()).activateSidePane(currentClass);
                    app.getGUI().updateToolbarControls(false);
                    currentClass.render();
                    currentClass.renderRelationships(data);
                }
                
            }
        }
    }

}