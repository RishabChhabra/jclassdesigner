/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd;

/**
 *
 * @author rishab
 */
public enum PropertyType
{
    PATH_SNAPSHOTS, 
    SNAP_LOCATION_TITLE,
    SNAP_SUCCESS_TITLE,
    SNAP_SUCCESS_MESSAGE,
    SNAP_FAILED_TITLE,
    SNAP_FAILED_MESSAGE,
    SELECT_ICON,
    SELECT_TOOLTIP,
    RESIZE_ICON,
    RESIZE_TOOLTIP,
    ADD_CLASS_ICON,
    ADD_CLASS_TOOLTIP,
    ADD_INTERFACE_ICON,
    ADD_INTERFACE_TOOLTIP,
    REMOVE_ICON,
    REMOVE_TOOLTIP,
    UNDO_ICON,
    UNDO_TOOLTIP,
    REDO_ICON,
    REDO_TOOLTIP,
    ZOOM_IN_ICON,
    ZOOM_IN_TOOLTIP,
    ZOOM_OUT_ICON,
    ZOOM_OUT_TOOLTIP,
    GRID_LABEL,
    SNAP_LABEL,
    CLASS_LABEL,
    PACKAGE_LABEL,
    PARENT_LABEL,
    VARIABLE_LABEL,
    ADD_ICON,
    ADD_VARIABLE_TOOLTIP,
    REMOVE_SIDE_ICON,
    REMOVE_VARIABLE_TOOLTIP,
    METHOD_LABEL,
    ADD_METHOD_TOOLTIP,
    REMOVE_METHOD_TOOLTIP,
    ADD_ARG_ICON, 
    ADD_ARG_TOOLTIP,
    REMOVE_ARG_ICON,
    REMOVE_ARG_TOOLTIP,
    INVALID_COMBO_TITLE, 
    INVALID_COMBO_MESSAGE,
    INVALID_NEW_COMBO_MESSAGE,
    INVALID_PARENT_TITLE,
    IVALID_PARENT_MESSAGE,
    PARENT_NOT_SAVED,
    GET_EXTERNAL_INFO_MESSAGE,
    GET_EXTERNAL_INFO_TITLE,
    INTERFACES_SKIPPED_MESSAGE,
    VARIABLE_ADD_ERROR,
    EMPTY_NAME_ERROR,
    NO_TYPE_ERROR,
    VARIABLE_NOT_SAVED
}